package graphicObjects;


import java.io.File;
import java.util.ArrayList;

import com.jogamp.opengl.util.texture.Texture;


public class TextureList extends ArrayList<Texture> {
	private static final long serialVersionUID = 2589240991464191809L;
	/* GraphicObjects
	 * 
	 * This is to hold all the instances of VBOs, TBOs, and IBOs.
	 * 
	 */

	public TextureList(){


	}

	public int indexOfImage( String imageName ){
		int i = 0;
		for ( File image : new File( "textures" ).listFiles() ) {
			if ( image.getName().equalsIgnoreCase( imageName ) )
				return i;
			i++;
		}
		
		return -1;
	}
	
	public Texture get( String imageName ){
		return get( indexOfImage( imageName ) );
	}

}