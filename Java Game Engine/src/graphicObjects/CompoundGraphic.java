package graphicObjects;

import java.util.ArrayList;

import maths.Vec3;

public class CompoundGraphic extends Graphic {

	private ArrayList<Graphic> _gs = new ArrayList<Graphic>();
	
	public CompoundGraphic( Vec3 initPos ) {
		super( -1, null, initPos );
		
	}
	
	public ArrayList<Graphic> getGraphics(){
		return this._gs;
		
	}

}
