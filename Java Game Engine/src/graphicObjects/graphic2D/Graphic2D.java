package graphicObjects.graphic2D;

import graphicObjects.Graphic;

import java.nio.FloatBuffer;

import com.jogamp.opengl.util.texture.Texture;

import main.Main;
import maths.Vec2;
import maths.Vec3;

/**
 * Graphic Object for 2D Graphics only.
 * 
 * @author Michael
 *
 */

public class Graphic2D extends Graphic implements Drawable2D {

	public Graphic2D(int graphicPos, Texture t, Vec2 initPos) {
		super(graphicPos, t, new Vec3(initPos));
		
	}

	@Override
	public void setCornerPosition(Vec2 p) {
		pos = new Vec3( p.x + this.scl.x , p.y - this.scl.y, 0.0f );
		
	}

	@Override
	public Vec2 getCornerPosition() {
		return new Vec2( this.pos.x - this.scl.x, this.pos.y + this.scl.y );
		
	}

	@Override
	public void setSizePos(Vec2 p) {
		this.scl = new Vec3( (this.pos.x - p.x)/2.0f, (this.pos.y - p.y)/2.0f, 1.0f );
		
	}

	@Override
	public void setDimensions( Vec2 d ) {
		this.scl.x = d.x;
		this.scl.y = d.y;
		
	}
	
	@Override
	public Vec2 getDimensions() {
		return new Vec2( this.scl.x, this.scl.y );
		
	}
	
	@Override
	public void setWidth(int w) {
		this.scl.x = w / 2.0f;
		
	}

	@Override
	public int getWidth() {
		return (int) (this.scl.x * 2.0f);
		
	}

	@Override
	public void setHeight(int h) {
		this.scl.y = h / 2.0f;
		
	}

	@Override
	public int getHeight() {
		return (int) (this.scl.y * 2.0f);
		
	}


	@Override
	public void setPosition(Vec2 p) {
		this.pos = new Vec3( p );
		
	}

	@Override
	public void addPosition(Vec2 p) {
		this.pos.add( new Vec3( p ) );
		
	}

	@Override
	public void setDrawType(int type) {
		this.drawType = type;
		
	}

	@Override
	public int getTBO_pos() {
		return Main._gl.get( graphicPos ).getTexBufferPos();
		
	}

	@Override
	public FloatBuffer getTexBuffer() {
		return Main._gl.get( graphicPos ).getTexBuffer();
		
	}

}
