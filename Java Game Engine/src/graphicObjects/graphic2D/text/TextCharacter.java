package graphicObjects.graphic2D.text;

import main.Main;
import maths.Vec2;
import graphicObjects.graphic2D.Graphic2D;
import graphicObjects.shapes.BasicSquare;

public class TextCharacter extends Graphic2D {
	
	private int fontSize = 12;
	
	private Vec2 d, pos;
	
	public static final int graphicPos = Main._gl.indexOfGraphic( BasicSquare.class );
	
	public TextCharacter( char c, float x, float y ) {
		super( graphicPos, Main._fl.getFromChar( c ), new Vec2() );
		
		pos = new Vec2( x, y );
		setFontSize( this.fontSize );
		
	}
	
	private void updateSize(){
		setDimensions( d );
		updatePos();
		
	}
	
	private void updatePos(){
		setPosition( new Vec2( pos.x + d.x, -pos.y - d.y ) );
		
	}
	
	public void setFontSize( int fontSize ) {
		this.fontSize = fontSize;
		d = new Vec2( fontSize * 7.0f/24.0f, fontSize * 5.0f/12.0f );
		updateSize();
		
	}
	
}
