package graphicObjects.graphic2D.text;

import java.io.File;
import java.util.ArrayList;

import com.jogamp.opengl.util.texture.Texture;

/**
 * 
 * Currently just ASCII, plans for UTF-8, but needs better stuffs.
 * 
 * @author Michael
 *
 */
public class FontList extends ArrayList<Texture> {
	private static final long serialVersionUID = -1740829340238477434L;
	
	
	public FontList() {
		
		
	}
	
	public Texture getFromChar( char c ) {
		
		int i = 0;
		for ( File f : new File( "Font" ).listFiles() ) {
			
			if ( f.getName().substring( 0, f.getName().length() - 4 ).equals( "" + (int) c ) ) {
				return this.get( i );
				
			}
			i++;
		}
		return null;
		
	}
	
}
