package graphicObjects.graphic2D.text;

import com.jogamp.opengl.util.texture.Texture;

import main.Main;
import maths.Vec2;
import maths.Vec3;
import graphicObjects.graphic2D.GraphicContainer;
import graphicObjects.shapes.BasicSquare;

/**
 * 
 * Several things need to be changed, esp the positioning and maybe the scaling.
 * 
 * @author Michael Mitchell
 *
 */
public class TextArea extends GraphicContainer {

	public static final int graphicPos = Main._gl.indexOfGraphic( BasicSquare.class );
	public static final Texture t = Main._tl.get( "WhiteSample.png" );

	private String text = "";

	public TextArea( int w, int h, int x, int y ){
		super( graphicPos, t, new Vec2() );
		scl = new Vec3( w, h, 1.0f );
		setCornerPosition( new Vec2( x, y ) );

	}

	public TextArea( float x, float y, String s ){
		super( graphicPos, t, new Vec2() );
		scl = new Vec3( (s.length() * 7.0f + 10.0f)/2.0f, 10.0f, 1.0f );
		setCornerPosition( new Vec2( x, y ) );
		setText( s );

	}

	private void updateText(){
		_gs.clear();
		int col = 0;
		for ( int i = 0 ; i < text.length() ; i++ ) {
			_gs.add( new TextCharacter( text.charAt( i ), col * 7.0f + 5.0f + getCornerPosition().x, 5.0f - getCornerPosition().y ) );
			col++;

		}
	}


	public void setText( String s ){
		this.text = s;
		updateText();

	}

	public String getText(){
		return this.text;

	}

}
