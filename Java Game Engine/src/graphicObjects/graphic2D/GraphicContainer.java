package graphicObjects.graphic2D;


import java.util.ArrayList;

import maths.Vec2;
import maths.Vec3;

import com.jogamp.opengl.util.texture.Texture;

public abstract class GraphicContainer extends Graphic2D {

	protected ArrayList<Graphic2D> _gs = new ArrayList<Graphic2D>();

	public GraphicContainer( int graphicPos, Texture t, Vec2 initPos ) {
		super(graphicPos, t, initPos);

	}

	public ArrayList<Graphic2D> getContainedGraphics(){
		return this._gs;

	}

	@Override
	public void setPosition( Vec3 p ){
		Vec3 dif = Vec3.subtract( p, getPosition() );
		for ( Graphic2D g : _gs ) {
			g.setPosition( Vec3.add( g.getPosition(), dif ) );
		}
		super.setPosition( p );

	}
	
	@Override
	public void setPosition( Vec2 p ){
		this.setPosition( new Vec3( p ) );

	}
	
	// TODO need scaling thing, similar to the position thing
	
	@Override
	public void setCornerPosition( Vec2 p ){
		setPosition( new Vec3( p.x + scl.x, p.y - scl.y, 0.0f ) );
		
	}
	
}