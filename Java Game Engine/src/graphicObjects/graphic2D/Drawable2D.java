package graphicObjects.graphic2D;

import maths.Vec2;

public interface Drawable2D {

	void setCornerPosition( Vec2 p );
	Vec2 getCornerPosition();

	void setSizePos( Vec2 p );

	void setDimensions( Vec2 d );
	Vec2 getDimensions();
	
	void setWidth( int w );
	int getWidth();

	void setHeight( int h );
	int getHeight();

	// General
	void setPosition( Vec2 p );
	void addPosition( Vec2 p );

}
