package graphicObjects.gui;

import main.Main;
import maths.Vec2;

import com.jogamp.opengl.util.texture.Texture;

import graphicObjects.graphic2D.Graphic2D;
import graphicObjects.shapes.BasicSquare;

public class GraphicalBorder extends Graphic2D {

	public static final int graphicPos = Main._gl.indexOfGraphic( BasicSquare.class );
	public static final Texture t = Main._tl.get( "BlackSample.png" );
	
	public GraphicalBorder() {
		super( graphicPos, t, new Vec2() );
		
	}
	
}
