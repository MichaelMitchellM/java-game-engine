package graphicObjects.gui;

import main.Main;
import maths.Vec2;
import maths.Vec3;

import com.jogamp.opengl.util.texture.Texture;

import graphicObjects.graphic2D.GraphicContainer;
import graphicObjects.shapes.BasicSquare;

public class GraphicalBorderedArea extends GraphicContainer {

	public static final int graphicPos = Main._gl.indexOfGraphic( BasicSquare.class );
	public static final Texture t = Main._tl.get( "WhiteSample.png" );

	private Vec2 d;
	private int borderSize = 5;

	public GraphicalBorderedArea( Vec2 p, Vec2 d ) {
		super( graphicPos, t, p );

		_gs.add( new GraphicalBorder() );    // Top
		_gs.add( new GraphicalBorder() );   // Bottom
		_gs.add( new GraphicalBorder() );  // Left
		_gs.add( new GraphicalBorder() ); // Right

		setDimensions( d );

	}

	public void update(){
		super.setScale( new Vec3( d.x/2.0f - borderSize, d.y/2.0f - borderSize, 1.0f ) );
		_gs.get( 0 ).setDimensions( new Vec2( d.x/2.0f, borderSize/2.0f ) );
		_gs.get( 0 ).setPosition( new Vec3( getPosition().x, getPosition().y + 0.5f * (d.y - borderSize), 0.0f ) );

		_gs.get( 1 ).setScale( new Vec3( d.x/2.0f, borderSize/2.0f, 1.0f ) );
		_gs.get( 1 ).setPosition( new Vec3( getPosition().x, getPosition().y - 0.5f * (d.y - borderSize), 0.0f ) );

		_gs.get( 2 ).setScale( new Vec3( borderSize/2.0f, d.y/2.0f, 1.0f ) );
		_gs.get( 2 ).setPosition( new Vec3( getPosition().x - 0.5f * (d.x - borderSize), getPosition().y, 0.0f ) );

		_gs.get( 3 ).setScale( new Vec3( borderSize/2.0f, d.y/2.0f, 1.0f ) );
		_gs.get( 3 ).setPosition( new Vec3( getPosition().x + 0.5f * (d.x - borderSize), getPosition().y, 0.0f ) );
		
	}
	
	public void setDimensions( Vec2 d ){
		this.d = d;
		update();

	}

	public void setInnerDimensions( Vec2 d ){
		this.d.x = d.x + 2*borderSize;
		this.d.y = d.y + 2*borderSize;
		update();

	}

	public void setBorderSize( int size ){
		this.borderSize = size;
		update();

	}

	public int getBorderSize(){
		return this.borderSize;

	}

	public Vec2 getDimensions(){
		return this.d;

	}

	public Vec2 getInnerDimensions(){
		return new Vec2( d.x - 2.0f * borderSize, d.y - 2.0f * borderSize );

	}
	
	@Override
	public void setScale( Vec3 s ){
		
		
	}
	
}