package graphicObjects.shapes;

import graphicObjects.GraphicalObject;

public class Sphere extends GraphicalObject {

	private int LoD;

	public Sphere( int LoD ) {
		super(
				getVerts( LoD ),
				getTex( LoD )
				);
		this.LoD = LoD;
	}

	public static float[] getVerts( int LoD ){
		// Is to fast to measure a single run at LoD of 8.
		// Is to fast to measure a single run at LoD of 16.
		// Between 0 - 16ms to run at LoD of 32.
		// Aprox. 16ms to run at LoD of 60.
		// Aprox. 150ms to run at LoD of 180.
		long start = System.currentTimeMillis();
		double rad = (2.0d*Math.PI)/LoD;
		float[] _spokeArray = new float[ ((LoD+1)*(LoD))*3 ];
		for ( int i = 0 ; i < LoD+1 ; i++ ) {
			for ( int j = 0 ; j < LoD ; j++ ) {
				_spokeArray[ ((i*LoD)+j)*3 + 0 ] = (float) (Math.sin( ( rad*i ) / 2.0d ) * Math.cos( rad*j ));  // x
				_spokeArray[ ((i*LoD)+j)*3 + 1 ] = (float) (Math.sin( ( rad*i ) / 2.0d ) * Math.sin( rad*j )); // y
				_spokeArray[ ((i*LoD)+j)*3 + 2 ] = (float) (Math.cos( ( rad*i ) / 2.0d ));                    // z
			}
		}
		float[] _vertArray = new float[ LoD*LoD*2*9 ];
		for ( int j = 0 ; j < LoD ; j++ ) {
			for ( int i = 0 ; i < LoD ; i++ ) {
				_vertArray[ (i*LoD*18) + (j*18) + 0 ] = _spokeArray[ LoD*3*(i+0) + (j+0)*3 + 0 ];
				_vertArray[ (i*LoD*18) + (j*18) + 1 ] = _spokeArray[ LoD*3*(i+0) + (j+0)*3 + 1 ];
				_vertArray[ (i*LoD*18) + (j*18) + 2 ] = _spokeArray[ LoD*3*(i+0) + (j+0)*3 + 2 ];
				if ( (j+1) == LoD ) {
					_vertArray[ (i*LoD*18) + (j*18) + 3 ] = _spokeArray[ LoD*3*(i+1) + 0 ];
					_vertArray[ (i*LoD*18) + (j*18) + 4 ] = _spokeArray[ LoD*3*(i+1) + 1 ];
					_vertArray[ (i*LoD*18) + (j*18) + 5 ] = _spokeArray[ LoD*3*(i+1) + 2 ];
				} else {
					_vertArray[ (i*LoD*18) + (j*18) + 3 ] = _spokeArray[ LoD*3*(i+1) + (j+1)*3 + 0 ];
					_vertArray[ (i*LoD*18) + (j*18) + 4 ] = _spokeArray[ LoD*3*(i+1) + (j+1)*3 + 1 ];
					_vertArray[ (i*LoD*18) + (j*18) + 5 ] = _spokeArray[ LoD*3*(i+1) + (j+1)*3 + 2 ];
				}
				_vertArray[ (i*LoD*18) + (j*18) + 6 ] = _spokeArray[ LoD*3*(i+1) + (j+0)*3 + 0 ];
				_vertArray[ (i*LoD*18) + (j*18) + 7 ] = _spokeArray[ LoD*3*(i+1) + (j+0)*3 + 1 ];
				_vertArray[ (i*LoD*18) + (j*18) + 8 ] = _spokeArray[ LoD*3*(i+1) + (j+0)*3 + 2 ];


				_vertArray[ (i*LoD*18) + (j*18) + 9  ] = _spokeArray[ LoD*3*(i+0) + (j+0)*3 + 0 ];
				_vertArray[ (i*LoD*18) + (j*18) + 10 ] = _spokeArray[ LoD*3*(i+0) + (j+0)*3 + 1 ];
				_vertArray[ (i*LoD*18) + (j*18) + 11 ] = _spokeArray[ LoD*3*(i+0) + (j+0)*3 + 2 ];

				if ( (j+1) == LoD ) {
					_vertArray[ (i*LoD*18) + (j*18) + 12 ] = _spokeArray[ LoD*3*(i+0) + 0 ];
					_vertArray[ (i*LoD*18) + (j*18) + 13 ] = _spokeArray[ LoD*3*(i+0) + 1 ];
					_vertArray[ (i*LoD*18) + (j*18) + 14 ] = _spokeArray[ LoD*3*(i+0) + 2 ];

					_vertArray[ (i*LoD*18) + (j*18) + 15 ] = _spokeArray[ LoD*3*(i+1) + 0 ];
					_vertArray[ (i*LoD*18) + (j*18) + 16 ] = _spokeArray[ LoD*3*(i+1) + 1 ];
					_vertArray[ (i*LoD*18) + (j*18) + 17 ] = _spokeArray[ LoD*3*(i+1) + 2 ];
				} else {
					_vertArray[ (i*LoD*18) + (j*18) + 12 ] = _spokeArray[ LoD*3*(i+0) + (j+1)*3 + 0 ];
					_vertArray[ (i*LoD*18) + (j*18) + 13 ] = _spokeArray[ LoD*3*(i+0) + (j+1)*3 + 1 ];
					_vertArray[ (i*LoD*18) + (j*18) + 14 ] = _spokeArray[ LoD*3*(i+0) + (j+1)*3 + 2 ];

					_vertArray[ (i*LoD*18) + (j*18) + 15 ] = _spokeArray[ LoD*3*(i+1) + (j+1)*3 + 0 ];
					_vertArray[ (i*LoD*18) + (j*18) + 16 ] = _spokeArray[ LoD*3*(i+1) + (j+1)*3 + 1 ];
					_vertArray[ (i*LoD*18) + (j*18) + 17 ] = _spokeArray[ LoD*3*(i+1) + (j+1)*3 + 2 ];
				}
			}
		}
		System.out.println( "Done! " + (System.currentTimeMillis()-start) );
		return _vertArray;
	}

	public static float[] getTex( int LoD ){
		float[] _texArray = new float[ LoD*LoD*6*2 ];
		float slice = (float) LoD; // get better at naming pls :S
		int index = 0;
		for ( int j = 0 ; j < LoD ; j++ ) {
			for ( int i = 0 ; i < LoD ; i++ ) {
				_texArray[ (12*index) + 0 ] = (i+0)/slice;
				_texArray[ (12*index) + 1 ] = (j+0)/slice;

				_texArray[ (12*index) + 2 ] = (i+1)/slice;
				_texArray[ (12*index) + 3 ] = (j+1)/slice;

				_texArray[ (12*index) + 4 ] = (i+0)/slice;
				_texArray[ (12*index) + 5 ] = (j+1)/slice;


				_texArray[ (12*index) + 6 ] = (i+0)/slice;
				_texArray[ (12*index) + 7 ] = (j+0)/slice;

				_texArray[ (12*index) + 8 ] = (i+1)/slice;
				_texArray[ (12*index) + 9 ] = (j+0)/slice;

				_texArray[ (12*index) + 10 ] = (i+1)/slice;
				_texArray[ (12*index) + 11 ] = (j+1)/slice;
				index++;
			}
		}
		return _texArray;
	}

	// Getter

	public int getLoD(){
		return LoD;
	}

}
