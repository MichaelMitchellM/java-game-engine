package graphicObjects.shapes;

import graphicObjects.GraphicalObject;

public class Line extends GraphicalObject {

	/* 
	 * Just a test class to make 3D lines from parametric vectors.
	 */
	
	public Line( int LoD, int start, int end ) {
		super(
				getVerts( LoD , start, end ),
				getTex( LoD )
				);
		
	}
	
	public static float[] getVerts( int LoD, int start, int end ){
		
		float[] _spoke = new float[ 3*(end-start)*LoD ];
		
		int i = 0;
		for ( float t = start ; t < end ; t+=(1.0f/LoD) ) {
			
			// Enter Parametric Equations Here :)
			
			/*X*/_spoke[ 3*i + 0 ] = (float) ( Math.cos( t ) );
			/*Y*/_spoke[ 3*i + 1 ] = (float) ( Math.sin( t ) );
			/*Z*/_spoke[ 3*i + 2 ] = (float) 0.0f;
			
			i++;
		}
		
		float[] _verts = new float[ 2 * _spoke.length ];
		for ( i = 0 ; i < (_spoke.length/3)-3 ; i++ ) {
			
			_verts[ 6*i + 0 ] = _spoke[ 3*i + 0 ];
			_verts[ 6*i + 1 ] = _spoke[ 3*i + 1 ];
			_verts[ 6*i + 2 ] = _spoke[ 3*i + 2 ];
			
			_verts[ 6*i + 3 ] = _spoke[ 3*(i+1) + 0 ];
			_verts[ 6*i + 4 ] = _spoke[ 3*(i+1) + 1 ];
			_verts[ 6*i + 5 ] = _spoke[ 3*(i+1) + 2 ];
			
		}
		
		return _verts;
	}
	
	public static float[] getTex( int LoD ){
		float[] _texArray = new float[ LoD*LoD*6*2 ];
		float slice = (float) LoD; // get better at naming pls :S
		for ( int i = 0 ; i < LoD ; i++ ) {
			for ( int j = 0 ; j < LoD ; j++ ) {
				_texArray[ (i*LoD*12) + (j*12) + 0 ] = (i+0)/slice;
				_texArray[ (i*LoD*12) + (j*12) + 1 ] = (j+0)/slice;

				_texArray[ (i*LoD*12) + (j*12) + 2 ] = (i+1)/slice;
				_texArray[ (i*LoD*12) + (j*12) + 3 ] = (j+1)/slice;

				_texArray[ (i*LoD*12) + (j*12) + 4 ] = (i+1)/slice;
				_texArray[ (i*LoD*12) + (j*12) + 5 ] = (j+0)/slice;


				_texArray[ (i*LoD*12) + (j*12) + 6 ] = (i+0)/slice;
				_texArray[ (i*LoD*12) + (j*12) + 7 ] = (j+0)/slice;

				_texArray[ (i*LoD*12) + (j*12) + 8 ] = (i+0)/slice;
				_texArray[ (i*LoD*12) + (j*12) + 9 ] = (j+1)/slice;

				_texArray[ (i*LoD*12) + (j*12) + 10 ] = (i+1)/slice;
				_texArray[ (i*LoD*12) + (j*12) + 11 ] = (j+1)/slice;
			}
		}
		return _texArray;
	}
	
}
