package graphicObjects.shapes;

import graphicObjects.GraphicalObject;

public class BasicSquare extends GraphicalObject {

	public static final float[] _verts = {
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		
		-1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		1.0f, 1.0f, 0.0f
	};
	
	public static final float[] _texs = {
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		
		0.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 1.0f
	};
	
	public BasicSquare(){
		super( _verts,
				_texs
				);

	}

}