package graphicObjects.shapes;

import graphicObjects.GraphicalObject;

public class Square extends GraphicalObject {

	public Square( int LoD ) {
		super(
				getVerts( LoD ),
				getTex( LoD )
				);
	}

	public static float[] getVerts( int LoD ){
		float[] _points = new float[ 3 * (LoD+1) * (LoD+1) ];
		float slice = (float) LoD;
		int index = 0;
		for ( float y = 1.0f ; y >= -1.0f  ; y-=(2.0f/slice) ) {
			for ( float x = -1.0f ; x <= 1.0f ; x+=(2.0f/slice) ) {
				/*X*/_points[ 3*index + 0 ] = x;
				/*Y*/_points[ 3*index + 1 ] = y;
				/*Z*/_points[ 3*index + 2 ] = 0.0f;
				index++;
			}
		}
		
		float[] _verts = new float[ 18 * LoD*LoD ];
		for ( int y = 0 ; y < LoD ; y++ ) {
			for ( int x = 0 ; x < LoD ; x++ ) {

				_verts[ (18*(y*(LoD) + x)) + 0 ] = _points[ (3*((LoD+1)*(y+0) + (x+0))) + 0 ];
				_verts[ (18*(y*(LoD) + x)) + 1 ] = _points[ (3*((LoD+1)*(y+0) + (x+0))) + 1 ];
				_verts[ (18*(y*(LoD) + x)) + 2 ] = _points[ (3*((LoD+1)*(y+0) + (x+0))) + 2 ];

				_verts[ (18*(y*(LoD) + x)) + 3 ] = _points[ (3*((LoD+1)*(y+1) + (x+0))) + 0 ];
				_verts[ (18*(y*(LoD) + x)) + 4 ] = _points[ (3*((LoD+1)*(y+1) + (x+0))) + 1 ];
				_verts[ (18*(y*(LoD) + x)) + 5 ] = _points[ (3*((LoD+1)*(y+1) + (x+0))) + 2 ];

				_verts[ (18*(y*(LoD) + x)) + 6 ] = _points[ (3*((LoD+1)*(y+1) + (x+1))) + 0 ];
				_verts[ (18*(y*(LoD) + x)) + 7 ] = _points[ (3*((LoD+1)*(y+1) + (x+1))) + 1 ];
				_verts[ (18*(y*(LoD) + x)) + 8 ] = _points[ (3*((LoD+1)*(y+1) + (x+1))) + 2 ];


				_verts[ (18*(y*(LoD) + x)) + 9  ] = _points[ (3*((LoD+1)*(y+0) + (x+0))) + 0 ];
				_verts[ (18*(y*(LoD) + x)) + 10 ] = _points[ (3*((LoD+1)*(y+0) + (x+0))) + 1 ];
				_verts[ (18*(y*(LoD) + x)) + 11 ] = _points[ (3*((LoD+1)*(y+0) + (x+0))) + 2 ];

				_verts[ (18*(y*(LoD) + x)) + 12 ] = _points[ (3*((LoD+1)*(y+1) + (x+1))) + 0 ];
				_verts[ (18*(y*(LoD) + x)) + 13 ] = _points[ (3*((LoD+1)*(y+1) + (x+1))) + 1 ];
				_verts[ (18*(y*(LoD) + x)) + 14 ] = _points[ (3*((LoD+1)*(y+1) + (x+1))) + 2 ];

				_verts[ (18*(y*(LoD) + x)) + 15 ] = _points[ (3*((LoD+1)*(y+0) + (x+1))) + 0 ];
				_verts[ (18*(y*(LoD) + x)) + 16 ] = _points[ (3*((LoD+1)*(y+0) + (x+1))) + 1 ];
				_verts[ (18*(y*(LoD) + x)) + 17 ] = _points[ (3*((LoD+1)*(y+0) + (x+1))) + 2 ];
			}
		}
		return _verts;
	}

	public static float[] getTex( int LoD ){
		float[] _texArray = new float[ 12* LoD*LoD ];
		float slice = (float) (LoD);
		int index = 0;
		for ( int j = LoD-1 ; j >= 0 ; j-- ) {
			for ( int i = 0 ; i < LoD ; i++ ) {
				_texArray[ (12*index) + 0 ] = (i+0)/slice;
				_texArray[ (12*index) + 1 ] = (j+0)/slice;

				_texArray[ (12*index) + 2 ] = (i+0)/slice;
				_texArray[ (12*index) + 3 ] = (j+1)/slice;

				_texArray[ (12*index) + 4 ] = (i+1)/slice;
				_texArray[ (12*index) + 5 ] = (j+1)/slice;


				_texArray[ (12*index) + 6 ] = (i+0)/slice;
				_texArray[ (12*index) + 7 ] = (j+0)/slice;

				_texArray[ (12*index) + 8 ] = (i+1)/slice;
				_texArray[ (12*index) + 9 ] = (j+1)/slice;

				_texArray[ (12*index) + 10 ] = (i+1)/slice;
				_texArray[ (12*index) + 11 ] = (j+0)/slice;
				index++;
			}
		}
		return _texArray;
	}
	
	
}
