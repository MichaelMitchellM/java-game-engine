package graphicObjects;

import graphicObjects.shapes.BasicSquare;
import graphicObjects.shapes.Line;
import graphicObjects.shapes.Sphere;
import graphicObjects.shapes.Square;

import java.util.ArrayList;

import ship.ModelShip;

public class GraphicList extends ArrayList<GraphicalObject> {
	private static final long serialVersionUID = 2389148059006671097L;
	/* GraphicObjects
	 * 
	 * This is to hold all the instances of VBOs, TBOs, and IBOs.
	 * 
	 */

	public GraphicList(){
		
		add( new Sphere( 16 ) );
		add( new Line( 64, -10, 10 ) );
		add( new BasicSquare() );
		add( new Square( 4 ) );
		add( new ModelShip() );
		
	}
	
	public GraphicalObject get( Class<?> c ){
		return get( indexOfGraphic( c ) );
	}
	
	public int indexOfGraphic( Class<?> c ){		
		for ( GraphicalObject o : this )
			if ( o.getClass().equals( c ) )
				return this.indexOf( o );
		return -1;
	}
	
}
