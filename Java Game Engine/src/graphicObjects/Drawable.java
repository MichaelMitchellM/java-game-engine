package graphicObjects;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.GL3;

import com.jogamp.opengl.util.texture.Texture;

import maths.FloatMatrix;
import maths.Vec3;


public interface Drawable {

	// General

	void draw( GL3 gl, int textureUnLoc, IntBuffer vbos, IntBuffer tbos );

	void setGraphic( int graphicPos );

	FloatMatrix getModelMatrix();

	FloatBuffer getVertBuffer();

	int getVBO_pos();

	void setPosition( Vec3 p );
	void addPosition( Vec3 p );
	Vec3 getPosition();

	void setRotation( Vec3 r );
	void addRotation( Vec3 r );
	Vec3 getRotation();

	void setScale( Vec3 s );
	void addScale( Vec3 s );
	Vec3 getScale();

	void setDrawType( int type );

	// Texture

	int getTBO_pos();

	FloatBuffer getTexBuffer();

	void setTexture( Texture t );
	Texture getTexture();

}
