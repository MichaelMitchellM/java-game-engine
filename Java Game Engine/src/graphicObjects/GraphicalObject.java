package graphicObjects;

import java.nio.FloatBuffer;

public abstract class GraphicalObject {

	protected int vboPos;
	protected int tboPos;

	protected FloatBuffer vertBuffer;
	protected FloatBuffer texBuffer;

	public GraphicalObject( float[] _vert, float[] _tex ){
		this.vertBuffer = FloatBuffer.wrap( _vert );
		this.texBuffer  = FloatBuffer.wrap( _tex );
	}

	public int getVertBufferPos(){
		return this.vboPos;
		
	}
	
	public int getTexBufferPos(){
		return this.tboPos;
		
	}
	
	public FloatBuffer getVertBuffer() {
		return this.vertBuffer;

	}

	public FloatBuffer getTexBuffer() {
		return this.texBuffer;

	}

}
