package graphicObjects;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.GL3;

import main.Main;
import maths.FloatMatrix;
import maths.Vec3;

import com.jogamp.opengl.util.texture.Texture;

public class Graphic implements Drawable {

	/* Graphic
	 * 
	 * Object to hold instance data for things.
	 * Graphics Yo.
	 * 
	 */

	protected Vec3 pos = new Vec3();
	protected Vec3 rot = new Vec3();
	protected Vec3 scl = new Vec3( 1.0f );

	protected int drawType = GL3.GL_TRIANGLES;

	protected int graphicPos;

	protected Texture t;

	public Graphic( int graphicPos, Texture t, Vec3 initPos ){
		this.graphicPos = graphicPos;
		this.t = t;
		this.pos = initPos;

	}

	@Override
	public void draw(GL3 gl, int textureUnLoc, IntBuffer vbos, IntBuffer tbos) {
		t.enable( gl );
		t.bind( gl );
		gl.glUniform1i( textureUnLoc, 0 );

		gl.glBindBuffer( GL3.GL_ARRAY_BUFFER, vbos.get( graphicPos ) );

		gl.glEnableVertexAttribArray( 0 );
		gl.glVertexAttribPointer( 0, 3, GL3.GL_FLOAT, false, 3 * 4, 0 );

		gl.glBindBuffer( GL3.GL_ARRAY_BUFFER, tbos.get( graphicPos ) );

		gl.glEnableVertexAttribArray( 1 );
		gl.glVertexAttribPointer( 1, 2, GL3.GL_FLOAT, false, 2 * 4, 0 );

		gl.glDrawArrays( drawType, 0, Main._gl.get( graphicPos ).vertBuffer.capacity() / 3 );

		t.disable( gl );
		gl.glDisableVertexAttribArray( 0 );
		gl.glDisableVertexAttribArray( 1 );

	}

	@Override
	public void setGraphic(int graphicPos) {
		this.graphicPos = graphicPos;

	}

	@Override
	public FloatMatrix getModelMatrix() {
		FloatMatrix modelScale = FloatMatrix.getScaleMatrix( scl );
		FloatMatrix modelRot   = FloatMatrix.getRotationMatrix( rot );
		FloatMatrix modelTrans = FloatMatrix.getTranslationMatrix( pos );
		return FloatMatrix.multiplyMatrix( FloatMatrix.multiplyMatrix(modelScale, modelRot), modelTrans );

	}

	@Override
	public FloatBuffer getVertBuffer() {
		return Main._gl.get( graphicPos ).getVertBuffer();

	}

	@Override
	public int getVBO_pos() {
		return Main._gl.get( graphicPos ).getVertBufferPos();

	}

	@Override
	public void setPosition(Vec3 p) {
		pos = p;

	}

	@Override
	public void addPosition(Vec3 p) {
		pos.add( p );

	}

	@Override
	public Vec3 getPosition() {
		return pos;

	}

	@Override
	public void setRotation(Vec3 r) {
		rot = r;

	}

	@Override
	public void addRotation(Vec3 r) {
		rot.add( r );

	}

	@Override
	public Vec3 getRotation(){
		return rot;

	}

	@Override
	public void setScale(Vec3 s) {
		scl = s;

	}

	@Override
	public void addScale(Vec3 s) {
		scl.add( s );

	}

	public Vec3 getScale(){
		return scl;

	}

	@Override
	public void setDrawType(int type) {
		drawType = type;

	}

	@Override
	public int getTBO_pos() {
		return Main._gl.get( graphicPos ).tboPos;

	}

	@Override
	public FloatBuffer getTexBuffer() {
		return Main._gl.get( graphicPos ).texBuffer;

	}

	@Override
	public void setTexture( Texture t ) {
		this.t = t;

	}

	@Override
	public Texture getTexture() {
		return t;

	}

}
