package physicalObjects;

import units.Units;
import universe.space.SpatialLocation;
import maths.Vec3;

public interface Spatial {

	SpatialLocation get_sLoc();
	
	Units getUnit();

	float getMass();
	
	Vec3 getForce();
	Vec3 getAcc();
	Vec3 getVel();
	
	
	
}
