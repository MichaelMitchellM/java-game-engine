package physicalObjects;

import icon.IconImages;
import icon.Iconable;
import physics.Kinematics;
import graphicObjects.Graphic;
import units.Units;
import universe.space.SpatialLocation;
import maths.Vec3;

public abstract class SpatialObject implements Physicsable, Spatial, Iconable {

	/* SpatialObject
	 * 
	 * In the future this class should also be able to handle the dimensions of the object.
	 * So we need a dimension system, but that is currently not important.
	 * 
	 */

	protected Graphic g = null;
	protected IconImages iconType = null; // TODO have a generic icon type.

	protected SpatialLocation sLoc;
	private Units measure; // The unit being used is gotten from this sLoc.getSpace().getUnit();

	private float mass; // Kg, always!

	protected Vec3 force = new Vec3(); // Newton, always!
	protected Vec3 acc   = new Vec3(); // Varies depending on the measure.
	protected Vec3 vel   = new Vec3(); // Varies depending on the measure.

	public SpatialObject( SpatialLocation sLoc, float mass, Vec3 vel ) {
		this.sLoc = sLoc;
		this.mass = mass;
		this.vel = vel;

		this.measure = sLoc.getSpace().getUnit();

	}

	public void setGraphic( Graphic g ) {
		this.g = g;

	}

	public Graphic getGraphic() {
		return this.g;

	}

	// Iconable

	@Override
	public void setIconImageType( IconImages type ) {
		this.iconType = type;

	}

	@Override
	public IconImages getIconImageType() {
		return this.iconType;

	};

	// Physicsable

	@Override
	public void setForce(Vec3 force) {
		this.force = force;
		updatePhysicsVars();
	}

	@Override
	public void addForce(Vec3 force) {
		this.force.add( force );
		updatePhysicsVars();
	}

	@Override
	public void updatePhysicsVars() {
		// This needs to account for the measure variable! Done :)
		// Newton = meters/second^2/Kg
		// Force = mass * acceleration
		// acceleration = Force/mass

		// By multiplying the mass by the number of meters in the measure variable
		// it bypasses having to do additional math after we get the acceleration in meters.
		// Nifty? I think so!
		// The since the conversion is just another scalar to multiply to the vector,
		// I combine them both for convenience.

		acc = Vec3.divide( force, mass );
	}

	@Override
	public void timeStep(float dT) {
		sLoc.addPos( Kinematics.get_dPos( vel, acc, dT ) );
		vel = Kinematics.get_fVel( vel, acc, dT );
		if ( g != null ) {
			g.setPosition( Vec3.divide( sLoc.getPos(), measure.numMeters() ) ); // TODO this needs work.
		}

	}

	// Spatial

	@Override
	public SpatialLocation get_sLoc() {
		return sLoc;
	}

	@Override
	public Units getUnit() {
		return measure;
	}

	@Override
	public float getMass(){
		return mass;
	}

	@Override
	public Vec3 getForce() {
		return force;
	}

	@Override
	public Vec3 getAcc() {
		return acc;
	}

	@Override
	public Vec3 getVel() {
		return vel;
	}

}
