package physicalObjects;

import maths.Vec3;

public interface Physicsable {
	
	
	void setForce( Vec3 force );
	void addForce( Vec3 force );
	
	void updatePhysicsVars();
	
	void timeStep( float dT );
	
}
