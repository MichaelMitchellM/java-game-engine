package icon;


public interface Iconable {
	
	String getInfo();
	
	void setIconImageType( IconImages type );
	IconImages getIconImageType();
	
}
