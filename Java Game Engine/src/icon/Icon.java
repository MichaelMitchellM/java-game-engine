package icon;


public class Icon {
	
	private Iconable ref;
	
	private IconImages imageType;
	private IconImage image;
	
	public Icon( Iconable ref ){
		this.ref = ref;
		this.imageType = ref.getIconImageType();
		
		image = new IconImage( this, imageType );
		
	}
	
	private void register(){
		
		
		
	}
	
	public Iconable getRef(){
		return this.ref;
		
	}
	
	public void setIconImageType( IconImages imageType ){
		this.imageType = imageType;
		
	}
	
	public IconImages getIconImageType(){
		return this.imageType;
		
	}
	
	public IconImage getIconImage(){
		return this.image;
		
	}
	
}
