package icon;

import main.Main;
import maths.Vec3;
import graphicObjects.Graphic;
import graphicObjects.shapes.BasicSquare;

public class IconImage extends Graphic {

	public static final int graphicPos = Main._gl.indexOfGraphic( BasicSquare.class );
	
	private IconImages type;
	
	public IconImage( Icon parent, IconImages type ){
		super( graphicPos, type.getTexture(), new Vec3() );
		
		setScale( new Vec3( 16.0f ) );
		
	}
	
	public void setType( IconImages type ){
		this.type = type;
		setTexture( type.getTexture() );
		
	}
	
	public IconImages getType(){
		return this.type;
		
	}
	
}