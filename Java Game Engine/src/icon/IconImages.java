package icon;

import main.Main;

import com.jogamp.opengl.util.texture.Texture;

public enum IconImages {
	
	Nebulae( "NebulaeIcon.png" ),
	Planet( "PlanetIcon.png" ),
	Star( "StarIcon.png" );
	
	private Texture t;
	
	private IconImages( String imageName ) {
		t = Main._tl.get( imageName );
		
	}
	
	public Texture getTexture(){
		return t;
		
	}
	
}
