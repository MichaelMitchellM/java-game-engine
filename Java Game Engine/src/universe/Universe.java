package universe;

import physicalObjects.SpatialObject;
import universe.celestial.Celestial;
import universe.cosmicObject.CosmicObject;
import universe.space.AbsoluteSpace;
import universe.space.Space;

public class Universe {
	
	private AbsoluteSpace aSpace = new AbsoluteSpace();
	
	public Universe(){
		
	}
	
	public void addShip( SpatialObject so) {
		aSpace.addShip( so );
		
	}
	
	public void addCosmic( CosmicObject co ){
		aSpace.addCosmic( co );
		
	}
	public void addCelestial( Celestial cel ){
		aSpace.addCelestial( cel );
		
	}
	
	public Space getSpace(){
		return aSpace;
		
	}
	
}