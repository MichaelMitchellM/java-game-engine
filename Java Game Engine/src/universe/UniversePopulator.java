package universe;

import maths.Vec3;
import universe.cosmicObject.galaxy.Galaxy;
import universe.cosmicObject.galaxy.GalaxyBuilder;
import universe.cosmicObject.galaxy.GalaxyGenerator;
import universe.space.SpatialLocation;

public class UniversePopulator {
	
	/* Generator
	 * 
	 * Will build the Verse object.
	 * Should take parameters to determine size, 
	 * but things should mainly be determined by what is found in our universe.
	 * Example:
	 * -> Avg. number of planets for solar systems,
	 *    Avg. number of binary stars per number of stars.
	 * 
	 * The idea is to make it as similar to reality as possible.
	 * 
	 */
	
	private Universe verse = new Universe();
	
	public UniversePopulator(){
		
		/* 1st test
		PlanetarySystemGenerator ssg = new PlanetarySystemGenerator( verse.getSpace() );
		PlanetarySystemBuilder ssb = ssg.getBuilder();
		ssb.set_sLoc( new SpatialLocation( verse.getSpace(), new Vec3() ) );
		PlanetarySystem s1 = ssb.generate();
		verse.addCosmic( s1 );
		*/
		
		GalaxyGenerator gg = new GalaxyGenerator( verse.getSpace() );
		GalaxyBuilder gb = gg.getBuilder();
		gb.set_sLoc( new SpatialLocation( verse.getSpace(), new Vec3() ) );
		Galaxy g1 = gb.generate();
		verse.addCosmic( g1 );
		
	}
	
	public Universe getVerse(){
		return this.verse;
	}
	
}
