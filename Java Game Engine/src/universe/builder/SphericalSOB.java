package universe.builder;

public abstract class SphericalSOB<E> extends SpatialObjectBuilder<E> {
	
	/* SphericalSOB
	 * 
	 * An abstract class for Spherical Spatial Object Building.
	 * e.g. Planets, Stars, moons, etc...
	 * 
	 */
	
	protected float radius;
	
	public SphericalSOB() {
		// TODO Auto-generated constructor stub
	}
	
	public void setRadius( float r ){
		this.radius = r;
	}
	
}
