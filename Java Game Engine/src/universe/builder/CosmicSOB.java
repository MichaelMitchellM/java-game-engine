package universe.builder;

import universe.space.RelativeSpace;

public abstract class CosmicSOB<E> extends SpatialObjectBuilder<E> {

	protected RelativeSpace rSpace;
	
	public CosmicSOB() {
		
		
		
	}
	
	public void set_rSpace( RelativeSpace r ){
		
		this.rSpace = r;
	}
	
	public RelativeSpace get_rSpace(){
		
		return this.rSpace;
	}
	
}
