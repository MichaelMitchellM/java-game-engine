package universe.builder;

import universe.space.SpatialLocation;
import maths.Vec3;

public abstract class SpatialObjectBuilder<E> {

	protected SpatialLocation sLoc;

	protected float totalMass;

	protected Vec3 startingVel = new Vec3();

	public SpatialObjectBuilder() {
		// TODO Auto-generated constructor stub
	}

	public abstract E generate();

	public void set_sLoc( SpatialLocation s ){
		
		this.sLoc = s;
	}
	
	public void setMass( float m ){
		// TODO limits on mass.
		this.totalMass = m;
	}

	public void setVel( Vec3 v ){
		// TODO limits on vel.
		this.startingVel = v;
	}

}
