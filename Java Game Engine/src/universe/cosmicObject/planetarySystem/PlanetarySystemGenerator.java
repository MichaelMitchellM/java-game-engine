package universe.cosmicObject.planetarySystem;

import maths.Vec3;
import units.Units;
import universe.celestial.planet.PlanetBuilder;
import universe.celestial.star.StarBuilder;
import universe.space.RelativeSpace;
import universe.space.Space;
import universe.space.SpatialLocation;

public class PlanetarySystemGenerator {

	private PlanetarySystemBuilder ssb = new PlanetarySystemBuilder();

	public PlanetarySystemGenerator( Space parentSpace ){

		ssb.set_rSpace( new RelativeSpace( parentSpace, Units.AU ) );

		/* Generation Algorithm
		 * 
		 * Star
		 * Determining the star will help set a number of variables.
		 * Star mass will determine the distances of planets from the star.
		 * Goldilocks zone etc.
		 * 
		 */



		// Do Star


		StarBuilder sb = new StarBuilder();

		sb.set_sLoc( new SpatialLocation( ssb.get_rSpace(), new Vec3() ) );
		sb.setMass( 2E30f );
		sb.setRadius( 695500000.0f );
		sb.setVel( new Vec3() );

		ssb.setStarBuilder( sb );

		// Do Planets

		for ( int i = 0 ; i < 9 ; i++ ) {

			PlanetBuilder pb = new PlanetBuilder();

			pb.set_sLoc(
					new SpatialLocation(
							ssb.get_rSpace(),
							new Vec3(
									(float) Math.pow( -1.0d,  i ) * (i+1) * ssb.get_rSpace().getUnit().numMeters(),
									(float) Math.pow( -1.0d,  i ) * (i+1) * ssb.get_rSpace().getUnit().numMeters(),
									(-i) * ssb.get_rSpace().getUnit().numMeters()
									) ) );
			pb.setMass( 5.974E24f );
			pb.setRadius( 6371000.0f );
			pb.setVel( new Vec3() );

			ssb.addPlanetBuilder( pb );

		}

	}

	public PlanetarySystemBuilder getBuilder(){
		return this.ssb;

	}


}
