package universe.cosmicObject.planetarySystem.planetarySystemLimits;

public class PlanetarySystemLimits {
	
	public enum Planets {
		
		MIN_PLANETS( 0 ),
		MAX_PLANETS( 9 );
		
		private int num;
		
		private Planets( int num ) {
			this.num = num;
			
		}
		
		public int value(){
			return num;
			
		}
		
	};
	
	
	
}
