package universe.cosmicObject.planetarySystem;

import java.util.ArrayList;

import universe.builder.CosmicSOB;
import universe.celestial.asteroid.Asteroid;
import universe.celestial.asteroid.AsteroidBuilder;
import universe.celestial.celestialEntity.asteroidBelt.AsteroidBelt;
import universe.celestial.celestialEntity.asteroidBelt.AsteroidBeltBuilder;
import universe.celestial.comet.Comet;
import universe.celestial.comet.CometBuilder;
import universe.celestial.planet.Planet;
import universe.celestial.planet.PlanetBuilder;
import universe.celestial.star.Star;
import universe.celestial.star.StarBuilder;

public class PlanetarySystemBuilder extends CosmicSOB<PlanetarySystem> {

	/* SolarSystemBuilder
	 * 
	 * The purpose of this class is to prepare objects and do calculations
	 * in before making a new SolarSystem object.
	 * 
	 */

	private StarBuilder sb;
	private AsteroidBeltBuilder abb;
	
	private ArrayList<PlanetBuilder>       _pb  = new ArrayList<PlanetBuilder>();
	private ArrayList<AsteroidBuilder>     _ab  = new ArrayList<AsteroidBuilder>();
	private ArrayList<CometBuilder>        _cb  = new ArrayList<CometBuilder>();


	public PlanetarySystemBuilder(){

		/*
		 * TODO The following needs changed!
		 * It is not adequate for dynamic generation and stuffs.
		 * 
		 * Stars will likely be limited to one,
		 * i.e.
		 * A binary star would be handled by a single star object.
		 * 
		 */

	}

	public PlanetarySystem generate(){

		// TODO Make sure essential variables are set... many others things too!
		// TODO Throw error when generation is attempted with out proper variables set.

		// Generate all the Objects from their builders:
		int i = 0;
		// STAR
		Star star = sb.generate();

		// Planet(s)
		Planet[] _planets = new Planet[ _pb.size() ];
		for ( PlanetBuilder p : _pb ) {

			_planets[ i ] = p.generate();
			i++;
		}

		// Asteroid Belt(s)
		AsteroidBelt asteroidBelt = null;
		
		

		// Asteroid(s)
		Asteroid[] _asteroids = new Asteroid[ _ab.size() ];
		i = 0;
		for ( AsteroidBuilder ab : _ab ) {



		}

		// Comet(s)
		Comet[] _comets = new Comet[ _cb.size() ];
		i = 0;
		for ( CometBuilder cb : _cb ) {



		}


		// Calculate totalMass
		totalMass += star.getMass();
		if ( asteroidBelt != null )
			totalMass += asteroidBelt.getMass();

		for ( Planet p : _planets )
			totalMass += p.getMass();

		for ( Asteroid a : _asteroids )
			totalMass += a.getMass();

		for ( Comet c : _comets )
			totalMass += c.getMass();

		return new PlanetarySystem(
				 sLoc,
				 rSpace,
				 totalMass,
				 startingVel,
				 star,
				 asteroidBelt,
				_planets,
				_asteroids,
				_comets
				);
	}

	public void setStarBuilder( StarBuilder star ){

		/* TODO make not bad T_T
		 * Procedure:
		 * 
		 * Limit stuff!
		 * 
		 */

		this.sb = star;

	}

	public void setAsteroidBuilder( Asteroid asteroid ){

		// TODO

	}
	
	public void addPlanetBuilder( PlanetBuilder p ){

		/* TODO make not bad T_T
		 * 
		 * Make sure planets are not too close.
		 * Make sure they have an appropriate mass.
		 * etc etc
		 * 
		 */

		_pb.add( p );

	}

	public void addAsteroidBeltBuilder( AsteroidBelt ab ){

		// TODO

	}

	public void addCometBuilder( Comet comet ){

		// TODO

	}

}
