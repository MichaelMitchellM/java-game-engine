package universe.cosmicObject.planetarySystem;

import graphicObjects.Graphic;
import graphicObjects.shapes.Sphere;

import javax.media.opengl.GL3;

import main.Main;
import maths.Vec3;
import universe.celestial.asteroid.Asteroid;
import universe.celestial.celestialEntity.asteroidBelt.AsteroidBelt;
import universe.celestial.comet.Comet;
import universe.celestial.planet.Planet;
import universe.celestial.star.Star;
import universe.cosmicObject.CosmicObject;
import universe.space.RelativeSpace;
import universe.space.SpatialLocation;

public class PlanetarySystem extends CosmicObject {

	/** Solar System
	 * What can a solar system have:
	 * 
	 * - Central star(s)
	 * - Planets
	 * - Asteroid Belt(s)
	 * - Lone Asteroids
	 * - Comet(s)
	 */

	private Star star;
	private AsteroidBelt asteroidBelt;
	
	private int numPlanets, numAsteroids, numComets;


	public PlanetarySystem(
			SpatialLocation sLoc,
			RelativeSpace   rSpace,
			float           totalMass,
			Vec3            vel,
			Star            star,
			AsteroidBelt    asteroidBelt,
			Planet[]       _planets,
			Asteroid[]     _asteroids,
			Comet[]        _comets
			) {
		super( sLoc, rSpace, totalMass, vel );

		// TODO This whole constructor needs to be... working :/
		rSpace.addCelestial( star );
		if ( asteroidBelt != null )
			rSpace.addCelestial( asteroidBelt );
		
		for ( Planet p : _planets )
			rSpace.addCelestial( p );
		for ( Asteroid a : _asteroids )
			rSpace.addCelestial( a );
		for ( Comet c : _comets )
			rSpace.addCelestial( c );
		
		this.star = star;

		this.numPlanets = (short) _planets.length;
		this.numAsteroids = (short) _asteroids.length;
		this.numComets = (short) _comets.length;

		Graphic g = new Graphic(
				Main._gl.indexOfGraphic( Sphere.class ),
				Main._tl.get( "RedSample.png" ),
				Vec3.divide( sLoc.getPos(), sLoc.getSpace().getUnit().numMeters() )
				);
		g.setScale( new Vec3( 1.0f ) );
		g.setDrawType( GL3.GL_LINES );
		setGraphic( g );

		// Set-up:

		/* Star(s)
		 * 
		 * -The solar system generator should calculate the orbits of the stars,
		 *  presuming there are more then one.
		 * 
		 * -Solar system must have one [if(_stars==null){error!}]
		 * -The star is the center of the solar system.
		 * -If there are multiple stars (Find out if in reality there are solar systems with more then 1 or 2 stars)
		 *  Find the middle point between the stars, that point will be what the stars rotate about.
		 *  The point will also be center point of the solar system.
		 * 
		 * -The star's mass (or the combined mass of several stars at the center point)
		 *  will determine the radius of the rSpace that the solar system will create for itself.
		 * 
		 */

		/* Planets
		 * 
		 * -Solar systems do not need planets to be viable.
		 * 
		 * -A planet should come with its orbit and the solar system generator
		 *  should take care to not make planets with dangerous orbits.
		 *  Example:
		 *  ->An orbit which planets collide
		 *  ->An orbit which planets gravity cause a planet to severely alter its orbit.
		 * 
		 * External Notes:
		 * - See Asteroid Belts
		 * 
		 */

		/* Asteroid Belts
		 * 
		 * -Solar systems do not need Asteroid Belts to be viable.
		 * 
		 * -In this context it would be a belt which orbits the sun directly.
		 * -Planets should be responsible for generating and managing their own asteroid belts.
		 * 
		 * -For now asteroid belts will be an entire loop, but in the future I would like them
		 *  to be able to have partial circles.
		 * 
		 */

		/* Lone Asteroids
		 * 
		 * -Solar systems do not need Lone Asteroids to be Viable.
		 * 
		 * Asteroids that fly around on their own, not bound to an asteroid belt.
		 * 
		 * Might have better ores then rocks in asteroid belts.
		 * 
		 */

		/* Comets
		 * 
		 * Just like lone asteroids, but different materials can be found and mined from them.
		 * 
		 */

	}


	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}

}
