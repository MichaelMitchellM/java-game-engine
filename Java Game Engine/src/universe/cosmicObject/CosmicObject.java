package universe.cosmicObject;

import maths.Vec3;
import physicalObjects.SpatialObject;
import universe.space.RelativeSpace;
import universe.space.SpatialLocation;

public abstract class CosmicObject extends SpatialObject {

	/* CosmicObject
	 * 
	 * Examples of CosmicObjects:
	 * -> Solar System
	 * -> Constellation
	 * -> Galaxy
	 * -> Galaxy Cluster
	 * 
	 * -> Nebulae
	 * 
	 */

	private RelativeSpace rSpace;

	public CosmicObject( SpatialLocation sLoc, RelativeSpace rSpace, float totalMass, Vec3 vel ){
		super( sLoc, totalMass, vel );
		
		this.rSpace = rSpace;
		
	}
	
	public RelativeSpace get_rSpace(){
		return this.rSpace;
	}
	
}
