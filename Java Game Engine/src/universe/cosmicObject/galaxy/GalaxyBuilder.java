package universe.cosmicObject.galaxy;

import java.util.ArrayList;

import universe.builder.CosmicSOB;
import universe.celestial.star.StarBuilder;

public class GalaxyBuilder extends CosmicSOB<Galaxy> {
	
	private ArrayList<StarBuilder> _sbs = new ArrayList<StarBuilder>();
	// TODO add other lists of objects.
//	private ArrayList<Nebulae> _nebula = new ArrayList<Nebulae>(); TODO make Nebulae class.
	
	public GalaxyBuilder() {
		
		
	}
	
	public void addStarBuilder( StarBuilder s ){
		_sbs.add( s );
		
	}
	
	@Override
	public Galaxy generate() {
		
		for ( StarBuilder sb : _sbs ) {
			rSpace.addCelestial( sb.generate() );
			
		}
		
		return new Galaxy( sLoc, rSpace, totalMass, startingVel );
		
	}
	
	
	
}
