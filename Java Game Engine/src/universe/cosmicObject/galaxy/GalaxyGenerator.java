package universe.cosmicObject.galaxy;

import java.util.Random;

import maths.Vec3;
import units.Units;
import universe.celestial.star.StarBuilder;
import universe.space.RelativeSpace;
import universe.space.Space;
import universe.space.SpatialLocation;

public class GalaxyGenerator {

	GalaxyBuilder gb = new GalaxyBuilder();

	public GalaxyGenerator( Space parentSpace ) {

		gb.set_rSpace( new RelativeSpace( parentSpace, Units.LIGHTYEAR ) );

		Random ran = new Random();

		for ( int i = 250 ; i != 0 ; i-- ) {

			StarBuilder sb = new StarBuilder();

			double r = 100.0d * ran.nextDouble();
			double t = 2.0d * Math.PI * ran.nextDouble();
			double p = Math.PI / 2.0d;
			
			float x = (float) (r * Math.sin( p ) * Math.cos( t ));
			float y = (float) (r * Math.sin( p ) * Math.sin( t ));
			float z = 25.0f * ran.nextFloat() - 25.0f * ran.nextFloat();
			
			sb.set_sLoc(
					new SpatialLocation(
							gb.get_rSpace(),
							new Vec3(
									x * gb.get_rSpace().getUnit().numMeters(),
									y * gb.get_rSpace().getUnit().numMeters(),
									z * gb.get_rSpace().getUnit().numMeters()
								)
							)
					);
			sb.setMass( 2E30f );
			//sb.setRadius( 6371000.0f );
			
			sb.setRadius( 2.0f*ran.nextFloat() + 0.25f);
			
			sb.setVel( new Vec3() );

			gb.addStarBuilder( sb );

		}

	}

	public GalaxyBuilder getBuilder(){
		return this.gb;

	}

}
