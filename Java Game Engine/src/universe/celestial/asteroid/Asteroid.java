package universe.celestial.asteroid;

import maths.Vec3;
import graphicObjects.shapes.Sphere;
import universe.celestial.Celestial;
import universe.space.SpatialLocation;

public class Asteroid extends Celestial {

	/* TODO
	 * 
	 * Should be able to mine Asteroids in the future.
	 * Which means we need to make a 'Mineable' interface
	 * and have different ore types.
	 * 
	 */
	
	public Asteroid( SpatialLocation sLoc, float mass, Vec3 vel ){
		super( sLoc, mass, vel );
		
		Sphere g = new Sphere( 8 );
	}

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}

}
