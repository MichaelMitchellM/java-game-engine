package universe.celestial.planet;

import icon.IconImages;
import graphicObjects.Graphic;
import graphicObjects.shapes.Sphere;

import javax.media.opengl.GL3;

import main.Main;
import maths.Vec3;
import universe.celestial.SphericalCelestial;
import universe.space.SpatialLocation;

public class Planet extends SphericalCelestial {

	public Planet( SpatialLocation sLoc, float mass, Vec3 vel, float radius ) {
		super( sLoc, mass, vel, radius );

		// Model
		
		Graphic g = new Graphic(
				Main._gl.indexOfGraphic( Sphere.class ),
				Main._tl.get( "earth.jpg" ),
				Vec3.divide( sLoc.getPos(), sLoc.getSpace().getUnit().numMeters()
						));

		g.setScale( Vec3.divide( new Vec3( radius ), sLoc.getSpace().getUnit().numMeters() ) );
		g.setScale( new Vec3( 0.5f ) );
		
		g.setDrawType( GL3.GL_TRIANGLES );

		setGraphic( g );

		// Icon
		
		setIconImageType( IconImages.Planet );
		
		/* TODO
		 * 
		 * Just set up all the things?
		 * 
		 */

	}

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}

}
