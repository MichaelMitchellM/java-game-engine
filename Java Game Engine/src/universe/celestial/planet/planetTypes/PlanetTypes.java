package universe.celestial.planet.planetTypes;

public enum PlanetTypes {
	
	TERRESTRIAL( 1000000.0f, 6371000.0f ) {
		
		
		
	},
	
	JOVIAN( 24622000.0f, 125839800.0f ),
	OTHER( 1000000.0f, 125839800.0f );
	
	private enum Terrestrial implements PlanetType {
		CARBON,
		CORELESS,
		DESERT,
		ICE,
		IRON,
		LAVA,
		OCEAN,
		SUBEARTH,
		SUPEREARTH;
	};
	
	private enum Jovian implements PlanetType {
		CHTHONIAN,
		ECCENTRIC_JUPITER,
		HELIUM,
		HOT_JUPITER,
		ICE_GIANT,
		MININEPTUNE,
		PUFFY,
		SUPERJUPITER;
	}
	
	private enum Other implements PlanetType {
		MASSIVE_SOLID,
		BROWN_DWARF,
		CIRCUMBINARY,
		DOUBLE,
		MESOPLANET,
		PLANETAR,
		PROTOPLANET,
		SUBBROWN_DWARF;
	}
	
	private PlanetTypes( float minRadius, float maxRadius ) {
		
		
		
	}
	
}
