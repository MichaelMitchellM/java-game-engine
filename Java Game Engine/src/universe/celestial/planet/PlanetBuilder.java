package universe.celestial.planet;

import universe.builder.SphericalSOB;

public class PlanetBuilder extends SphericalSOB<Planet> {
	
	/* TODO
	 * 
	 * Add the variables and stuff!
	 * - PlanetType type
	 */
	
	public PlanetBuilder() {
		
		
		
	}
	
	@Override
	public Planet generate() {
		
		
		return new Planet( sLoc, totalMass, startingVel, radius );
	}
	
}
