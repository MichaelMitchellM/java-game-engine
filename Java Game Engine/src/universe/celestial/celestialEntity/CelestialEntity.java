package universe.celestial.celestialEntity;

import maths.Vec3;
import universe.celestial.Celestial;
import universe.space.SpatialLocation;

public abstract class CelestialEntity extends Celestial {
	
	/* Celestial Entity
	 * 
	 * A Celestial Entity will be defined as a group of Celestials
	 * that are not big enough to be a CosmicObject,
	 * but to large for all the Celestials it contains to be counted individually.
	 * 
	 * The original reason for creating this is to handle asteroid belts.
	 * Example:
	 * An asteroid belt could contain millions of individual asteroids.
	 * It would be impossible to have all of those as their own entity and do a physics calculation each step.
	 * At least with todays computers.
	 * 
	 * Some thoughts:
	 * -> Unsure how to handle dimensions.
	 * 
	 */
	
	public CelestialEntity( SpatialLocation sLoc, float mass, Vec3 vel ) {
		super( sLoc, mass, vel );
	}
	
	
}
