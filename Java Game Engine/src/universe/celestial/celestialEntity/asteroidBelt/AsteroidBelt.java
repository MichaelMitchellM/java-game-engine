package universe.celestial.celestialEntity.asteroidBelt;

import maths.Vec3;
import universe.celestial.celestialEntity.CelestialEntity;
import universe.space.SpatialLocation;

public class AsteroidBelt extends CelestialEntity {
	
	/*
	 * 
	 * Self explanatory...
	 * For belts around:
	 * - the sun(s)
	 * - planets
	 * 
	 * Thoughts:
	 * -> I am unsure how to manage a belt, should it be a collection
	 *    of pockets of asteroids orbiting a single thing,
	 *    or just have a single disk.
	 * 
	 * 
	 */
	
	public AsteroidBelt( SpatialLocation sLoc, float mass, Vec3 vel ) {
		super( sLoc, mass, vel );
	}

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
