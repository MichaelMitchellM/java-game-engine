package universe.celestial;

import maths.Vec3;
import universe.space.SpatialLocation;

public abstract class SphericalCelestial extends Celestial {

	private float radius;
	
	public SphericalCelestial( SpatialLocation sLoc, float mass, Vec3 vel, float radius ) {
		super(sLoc, mass, vel);
		this.radius = radius;
		
	}
	
	public float getRadius(){
		return this.radius;
		
	}
	
}
