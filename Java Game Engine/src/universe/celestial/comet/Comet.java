package universe.celestial.comet;

import maths.Vec3;
import graphicObjects.shapes.Sphere;
import universe.celestial.Celestial;
import universe.space.SpatialLocation;

public class Comet extends Celestial {
	
	/* TODO
	 * 
	 * Should be an object you can mine in the future
	 * 
	 * Look at Asteroid for more thoughts on this.
	 * 
	 * Comets should have special resources, different then asteroids.
	 * 
	 */
	
	public Comet( SpatialLocation sLoc, float mass, Vec3 vel ) {
		super( sLoc, mass, vel );
		
		Sphere g = new Sphere( 8 ); // TODO graphic needs to change.
		
	}

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
