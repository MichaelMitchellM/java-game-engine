package universe.celestial;

import maths.Vec3;
import physicalObjects.SpatialObject;
import universe.space.SpatialLocation;

public abstract class Celestial extends SpatialObject {
	
	public Celestial( SpatialLocation sLoc, float mass, Vec3 vel ){
		super( sLoc, mass, vel );
		
	}
	
	
}
