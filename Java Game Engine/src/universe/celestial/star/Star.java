package universe.celestial.star;

import icon.IconImages;

import javax.media.opengl.GL3;

import graphicObjects.Graphic;
import graphicObjects.shapes.Sphere;
import main.Main;
import maths.Vec3;
import universe.celestial.SphericalCelestial;
import universe.space.SpatialLocation;

public class Star extends SphericalCelestial {

	private StarTypes type;

	public Star( SpatialLocation sLoc, float mass, Vec3 vel, float radius ){
		super( sLoc, mass, vel, radius );

		// Graphic
		Graphic g = new Graphic(
				Main._gl.indexOfGraphic( Sphere.class ),
				Main._tl.get( "sun.jpg" ),
				Vec3.divide( sLoc.getPos(), sLoc.getSpace().getUnit().numMeters()
						));

		g.setScale( Vec3.divide( new Vec3( radius ), sLoc.getSpace().getUnit().numMeters() ) );
		//g.setScale( new Vec3( 0.5f ) );
		g.setScale( new Vec3( radius ) );

		g.setDrawType( GL3.GL_TRIANGLES );

		setGraphic( g );

		// Icon

		setIconImageType( IconImages.Star );

	}

	public StarTypes getType(){
		return this.type;

	}

	@Override
	public String getInfo() {
		return "" + get_sLoc().get_rPos();
	}

}