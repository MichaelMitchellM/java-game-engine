package universe.celestial.star;

public enum StarTypes {

	SPECTRAL_CLASS( Stars.O, Stars.B, Stars.A, Stars.F, Stars.G, Stars.K, Stars.M ),
	EXTENDED_SPECRTAL( Stars.WR, Stars.C, Stars.D ),
	;

	private Stars[] _stars;

	private StarTypes( Stars ...s ) {
		this._stars = s;

	}

	public Stars[] getStars(){
		return this._stars;

	}

	private enum Stars {

		// Spectral Classification
		O,
		B,
		A,
		F,
		G,
		K,
		M,

		// Extended Spectral

		WR,
		C,
		D,
		;

	};

}
