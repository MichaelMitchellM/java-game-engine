package universe.celestial.star;

import universe.builder.SphericalSOB;

public class StarBuilder extends SphericalSOB<Star> {

	/*
	 * Add color and other silly variables laterz.
	 */

	public StarBuilder() {
		
		
		
	}

	@Override
	public Star generate() {
		return new Star( sLoc, totalMass, startingVel, radius );
		
	}
	
}
