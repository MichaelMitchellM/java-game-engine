package universe.space;

import units.Units;

public class AbsoluteSpace extends Space {
	
	// Contains Cosmic Objects.
	
	public AbsoluteSpace(){
		super( Units.LIGHTYEAR ); // TODO Needs to be LY or Parsecs (add Parsecs to _ul)
	}
	
}
