package universe.space;

import java.util.ArrayList;

import main.Projectile;
import physicalObjects.SpatialObject;
import units.Units;
import universe.celestial.Celestial;
import universe.cosmicObject.CosmicObject;

public interface Spacious {
	
	void addShip( SpatialObject so );
	void addProj( Projectile po );
	
	void addCosmic( CosmicObject co );
	void addCelestial( Celestial c );
	
	boolean containsShips();
	boolean containsProj();
	
	boolean containsCosmics();
	boolean containsCelestials();
	boolean containsStuff();
	
	ArrayList<SpatialObject> getAllSpatialObjects();
	
	ArrayList<SpatialObject> getShips();
	ArrayList<Projectile> getProj();
	
	ArrayList<CosmicObject> getCosmics();
	ArrayList<Celestial> getCelestials();
	ArrayList<SpatialObject> getMassive();
	
	Units getUnit();
	
}
