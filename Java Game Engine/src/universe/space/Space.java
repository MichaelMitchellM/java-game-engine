package universe.space;

import graphicObjects.Graphic;

import java.util.ArrayList;

import main.Main;
import main.Projectile;
import maths.Vec3;
import physicalObjects.SpatialObject;
import physics.Gravity;
import units.Units;
import universe.celestial.Celestial;
import universe.cosmicObject.CosmicObject;

public abstract class Space implements Spacious {

	/* Space
	 * 
	 * If we want to allow the ability to change the unit of measure,
	 * we will have to go through all objects contained and change their positions to match the update.
	 * 
	 * This will only be added if the time comes when this would be needed,
	 * for now I cannot see a situation when this would be used.
	 * 
	 */

	private Units measure;

	private ArrayList<SpatialObject> _ships = new ArrayList<SpatialObject>();
	private ArrayList<Projectile>    _proj  = new ArrayList<Projectile>();
	
	private ArrayList<CosmicObject>  _co      = new ArrayList<CosmicObject>();
	private ArrayList<Celestial>     _cel     = new ArrayList<Celestial>();
	private ArrayList<SpatialObject> _massive = new ArrayList<SpatialObject>();

	//private ArrayList<Celestial>

	//ArrayList<> will be used for like ships and stuff.

	public Space( Units u ) {
		this.measure = u;
		
	}

	public ArrayList<Graphic> getAllGraphics() {
		ArrayList<SpatialObject> _sos = getAllSpatialObjects();
		ArrayList<Graphic> _gs = new ArrayList<Graphic>( _sos.size() );
		for ( int i = 0 ; i < _sos.size() ; i++ )
			if ( _sos.get( i ).getGraphic() != null )
				_gs.add( _sos.get( i ).getGraphic() );
		return _gs;
		
	}
	
	public void calculate( float dT ) {
		ArrayList<SpatialObject> _allObjects = new ArrayList<SpatialObject>();
		_allObjects.addAll( _cel );
		_allObjects.addAll( _co  );
		for ( SpatialObject o : _allObjects ) {
			Vec3 FG = new Vec3();
			for ( SpatialObject so : getMassive() ) {
				if ( !so.equals( o ) ) {
					Vec3 OtoSO = Vec3.subtract( so.get_sLoc().getPos(), o.get_sLoc().getPos() );
					float forceOfG = Gravity.getFG(
							so.getMass(),
							o.getMass(),
							OtoSO.getMagnitude()
							);
					FG.add( Vec3.multiply( OtoSO.getUnitVec3(), forceOfG ) );
				}
			}
			o.setForce( FG ); // Sets the external forces, gravity.
			o.timeStep( dT );
		}
		for ( CosmicObject co : getCosmics() ) {
			co.get_rSpace().calculate( dT );
		}
		
	}

	@Override
	public void addShip( SpatialObject so ) {
		_ships.add( so );
		
	}
	
	@Override
	public void addProj( Projectile po ) {
		if ( _proj.size() > 100 ) { // TODO this needs to be changed. This is to prevent lag. Since things cannot remove themselves with out thread issues and stuff. idk
			for ( Projectile p : _proj )
				Main._scene.remove( p.getGraphic() );
			_proj.clear();
			System.out.println( "Projectiles Cleared due to excesive amount!" );
			
		}
		_proj.add( po );
		
	}
	
	@Override
	public void addCosmic( CosmicObject co ) {
		_co.add( co );
		_massive.add( co ); // TODO This should be made to only hold 'massive' objects. Relative to things ofc.
		
	}

	@Override
	public void addCelestial( Celestial c ) {
		_cel.add( c );
		_massive.add( c ); // TODO This should be made to only hold 'massive' objects.
		
	}

	@Override
	public boolean containsShips() {
		return this._ships.isEmpty();
		
	}
	
	@Override
	public boolean containsProj() {
		return this._proj.isEmpty();
		
	}
	
	@Override
	public boolean containsCosmics() {
		return this._co.isEmpty();
		
	}

	@Override
	public boolean containsCelestials() {
		return this._cel.isEmpty();
		
	}

	@Override
	public boolean containsStuff() { // For the time when we can have player things.
		return false;
		
	}

	@Override
	public ArrayList<SpatialObject> getAllSpatialObjects() {
		ArrayList<SpatialObject> _all = new ArrayList<SpatialObject>( _co.size() + _cel.size() );
		_all.addAll( _ships );
		_all.addAll( _proj );
		
		_all.addAll( _co );
		_all.addAll( _cel );
		return _all;
		
	}
	
	@Override
	public ArrayList<SpatialObject> getShips() {
		return this._ships;
		
	}
	
	@Override
	public ArrayList<Projectile> getProj() {
		return this._proj;
		
	}
	
	@Override
	public ArrayList<CosmicObject> getCosmics() {
		return this._co;
		
	}

	@Override
	public ArrayList<Celestial> getCelestials() {
		return this._cel;
		
	}

	@Override
	public ArrayList<SpatialObject> getMassive() {
		return this._massive;
		
	}

	@Override
	public Units getUnit() {
		return this.measure;
		
	}

}
