package universe.space;

import maths.Vec3;

public class SpatialLocation {

	private Space s;
	private Vec3 pos;

	public SpatialLocation( Space s, Vec3 pos ){
		this.s = s;
		this.pos = pos;
		
	}

	// Info

	public boolean isAbsLoc(){
		if ( s instanceof AbsoluteSpace )
			return true;
		else
			return false;
		
	}

	// Position
	public Vec3 getPos(){
		return this.pos;
		
	}

	public void setPos( Vec3 p ){
		this.pos = p;
		
	}

	public void addPos( Vec3 p ){
		this.pos.add( p );
		
	}

	public Vec3 get_rPos(){
		return Vec3.divide( this.pos, this.s.getUnit().numMeters() );
		
	}
	

	// Space
	public Space getSpace(){
		return this.s;
		
	}
}
