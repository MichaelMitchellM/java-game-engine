package universe.space;

import units.Units;

public class RelativeSpace extends Space {

	// rSpace is a pocket of space for CosmicObjects like Galaxies and SolarSystems.
	// and maybe in the future planets and some other Celestial can have their own space.

	private Space parentSpace;

	public RelativeSpace( Space parent, Units u ){
		super( u );
		this.parentSpace = parent;
	}

	public Space getParentSpace(){
		return this.parentSpace;
	}
	
	// TODO will need a way to change the parent space if like a star leaves a galaxy or something crazy.
	
}
