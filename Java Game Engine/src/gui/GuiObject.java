package gui;

import maths.Vec2;
import maths.Vec3;
import interaction.Interactable;
import graphicObjects.graphic2D.Graphic2D;

public abstract class GuiObject implements Interactable {

	private Vec2 p, d;
	private Graphic2D g;

	public GuiObject( Graphic2D g, Vec2 p, Vec2 d ) {
		this.g = g;
		this.p = p;
		this.d = d;

	}

	public void setGraphic( Graphic2D g ){
		this.g = g;
		// TODO apply the size and position to the new graphic.

	}

	public Graphic2D getGraphic(){
		return this.g;

	}

	// Interactable
	@Override
	public void setCornerPosition( Vec2 p ) {
		setPosition( new Vec2( p.x + d.x/2.0f, p.y - d.y/2.0f ));

	}

	@Override
	public void setInnerCornerPosition( Vec2 p ) {
		setPosition( this.p = new Vec2( p.x + d.x/2.0f, p.y - d.y/2.0f ) );

	}

	@Override
	public void setPosition( Vec2 p ) {
		this.p = p;
		g.setPosition( p );

	}

	@Override
	public void setDimensions( Vec2 d ) {
		this.d = d;
		g.setScale( new Vec3( d.x/2.0f, d.y/2.0f, 1.0f ) );

	}

	@Override
	public void setInnerDimensions( Vec2 d ) {
		setDimensions( d );

	}

	@Override
	public Vec2 getCornerPosition() {
		return new Vec2( p.x - d.x/2.0f, p.y + d.y/2.0f );

	}

	@Override
	public Vec2 getInnerCornerPosition() {
		return new Vec2( p.x - d.x, p.y + d.y );

	}

	@Override
	public Vec2 getPosition() {
		return this.p;

	}

	@Override
	public Vec2 getDimensions() {
		return this.d;

	}

	@Override
	public Vec2 getInnerDimensions() {
		return this.d;

	}

}