package gui;

import java.util.ArrayList;

import main.Resizable;

/**
 * 
 * Class for the in game gui.
 * 
 * Will have areas that can be changed depending on items selected.
 * Might have to finally make the CompoundGraphic Class.
 * 
 * Gui Sections:
 * |- Actions (all units will have actions they can do).
 * |- Notification list.
 * |- Other things?
 *  
 * @author Michael Mitchell
 * 
 */

public class GUI implements Resizable {

	private ArrayList<GuiObject> _gs = new ArrayList<GuiObject>();
	
	public GUI() {
		
		
		
	}

	// Resizable
	@Override
	public void update( int width, int height ) {
		
		

	}
	
	public void addGuiObject( GuiObject go ){
		_gs.add( 0, go );
		
	}
	
	public ArrayList<GuiObject> getGuiObjects(){
		return this._gs;
		
	}

}