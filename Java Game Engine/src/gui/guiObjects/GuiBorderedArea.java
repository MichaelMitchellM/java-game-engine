package gui.guiObjects;

import maths.Vec2;
import graphicObjects.gui.GraphicalBorderedArea;
import gui.GuiObject;

public abstract class GuiBorderedArea extends GuiObject {

	private int borderSize = 5;

	public GuiBorderedArea( Vec2 p, Vec2 d ) {
		super( new GraphicalBorderedArea( p, d ), p, d );

	}

	public void setBorderSize( int s ){
		this.borderSize = s;
		((GraphicalBorderedArea) getGraphic()).setBorderSize( s );

	}

	// Interactable
	@Override
	public void setInnerCornerPosition( Vec2 p ) {
		setPosition(
				new Vec2(
						p.x - getDimensions().x/2.0f - ((GraphicalBorderedArea) getGraphic()).getBorderSize(),
						p.y + getDimensions().y/2.0f + ((GraphicalBorderedArea) getGraphic()).getBorderSize()
						)
				);

	}

	@Override
	public void setInnerDimensions( Vec2 d ) {
		setDimensions(
				new Vec2(
						d.x + 2*borderSize,
						d.y + 2*borderSize
						)
				);

	}

	@Override
	public Vec2 getInnerCornerPosition() {
		// TODO not sure this works :/
		return new Vec2(
				getPosition().x - getDimensions().x/2.0f + ((GraphicalBorderedArea) getGraphic()).getBorderSize(),
				getPosition().y + getDimensions().y/2.0f - ((GraphicalBorderedArea) getGraphic()).getBorderSize()
				);

	}

	@Override
	public Vec2 getInnerDimensions(){
		return new Vec2(
				getDimensions().x - 2*borderSize,
				getDimensions().y - 2*borderSize
				);
		
	}
	
}