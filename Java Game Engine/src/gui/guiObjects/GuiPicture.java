package gui.guiObjects;

import com.jogamp.opengl.util.texture.Texture;

import main.Main;
import maths.Vec2;
import maths.Vec3;
import graphicObjects.graphic2D.Graphic2D;
import graphicObjects.shapes.BasicSquare;
import gui.GuiObject;

public class GuiPicture extends GuiObject {

	public GuiPicture( Texture t, Vec2 p, Vec2 d ) {
		super( new Graphic2D( Main._gl.indexOfGraphic( BasicSquare.class ), t, p ), p, d );
		
		getGraphic().setScale( new Vec3( d, 1.0f ) );
		
	}

	@Override
	public void interactAction() {
		// TODO Auto-generated method stub
		
	}

}
