package gui.guiObjects.bordered;

import graphicObjects.graphic2D.GraphicContainer;
import graphicObjects.graphic2D.text.TextArea;
import gui.guiObjects.GuiBorderedArea;
import maths.Vec2;

public class GuiBorderedButton extends GuiBorderedArea {

	private boolean hasTextArea = false;
	private String text = "";

	public GuiBorderedButton( Vec2 p, Vec2 d ) {
		super( p, d );

	}

	public GuiBorderedButton( String text, Vec2 p, Vec2 d ) {
		this( p, d );
		this.text = text;
		setText( text );

	}

	@Override
	public void interactAction() {



	}

	public void setText( String text ){
		this.text = text;
		if ( !hasTextArea ) {
			((GraphicContainer) getGraphic()).getContainedGraphics().add( new TextArea( getInnerCornerPosition().x, getInnerCornerPosition().y, text ) );
			hasTextArea = true;
		} else {
			((GraphicContainer) getGraphic()).getContainedGraphics().set( 4, new TextArea( getInnerCornerPosition().x, getInnerCornerPosition().y, text ) );
		}
		
	}

	public String getText(){
		return this.text;

	}

}