package gui.guiObjects.bordered;

import java.util.LinkedList;

import gui.GuiObject;
import gui.guiObjects.GuiBorderedArea;
import main.Main;
import maths.Vec2;

public class GuiBorderedList extends GuiBorderedArea {

	private LinkedList<GuiObject> _gos = new LinkedList<GuiObject>();

	public GuiBorderedList( Vec2 p, Vec2 d ) {
		super( p, d );

	}

	public void addGuiObject( GuiObject go ){
		// TODO set position of the go in next available menu spot?

		go.setDimensions( getInnerDimensions() );
		go.setCornerPosition( new Vec2( getInnerCornerPosition().x, getInnerCornerPosition().y ) );

		_gos.add( go );
		Main.gui.addGuiObject( go );

	}

	public LinkedList<GuiObject> getGuiObjects(){
		return this._gos;

	}

	// Interactable
	@Override
	public void interactAction() {
		getGraphic().setTexture( Main._tl.get( "RedSample.png" ) );

	}

	@Override
	public void setPosition( Vec2 p ){
		Vec2 dif = Vec2.subtract( p, getPosition() );
		for ( GuiObject go : _gos ) {
			go.setPosition( Vec2.add( go.getPosition(), dif ) );
		}
		super.setPosition( p );

	}

}