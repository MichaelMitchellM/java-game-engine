package ship;

import graphicObjects.Graphic;

import com.jogamp.opengl.math.Quaternion;

import main.Main;
import main.Projectile;
import maths.FloatMatrix;
import maths.Vec3;
import physicalObjects.SpatialObject;
import universe.space.SpatialLocation;

public class Ship extends SpatialObject {

	private Vec3 dir = new Vec3( 0.0f, 0.0f, -1.0f );
	private Quaternion ori = new Quaternion( new float[]{ 0.0f, 1.0f, 0.0f }, 0.0f );

	private long lastShot = System.currentTimeMillis(); // TODO this is kinda lame, find a better way.
	
	public Ship( SpatialLocation sLoc ) {
		super( sLoc, 1E3f, new Vec3() );

		Graphic g = new GraphicalShip( new Vec3() );
		setGraphic( g );
		
	}

	// Orientation
	
	public void addRot( Vec3 rot ) {
		Quaternion x = new Quaternion( new float[]{ 0.0f, 1.0f, 0.0f }, rot.y );
		x.mult( ori );
		ori = x;
		Quaternion y = new Quaternion( new float[]{ 1.0f, 0.0f, 0.0f }, rot.x );
		y.mult( ori );
		ori = y;
		Quaternion z = new Quaternion( new float[]{ 0.0f, 0.0f, 1.0f }, rot.z );
		z.mult( ori );
		ori = z;
		
		FloatMatrix oriRot = new FloatMatrix( ori.toMatrix(), 4, 4 );
		float[] f = FloatMatrix.multiplyMatrix( oriRot, new FloatMatrix( new float[]{ 0.0f, 0.0f, -1.0f, 0.0f }, 4, 1 ) ).getArray();
		dir = new Vec3( f[ 0 ], f[ 1 ], f[ 2 ] );

	}

	public Quaternion getOrientation() {
		return this.ori;
		
	}
	
	// Movement
	
	/**
	 * Applied force in the opposite direction of the velocity vectors.
	 * It is like retro rockets, but not really.
	 */
	public void applyJello(){
		// TODO don't use magic numbers :/
		Vec3 v = getVel();
		setForce( Vec3.multiply( v, -1E4f ) );
		
	}
	
	public void setForward( float f ) {
		setForce( Vec3.multiply( dir, f ) );
		
	}
	
	public void addForward( float f ) {
		addForce( Vec3.multiply( dir, f ) );

	}
	
	public void setSide( float f ) {
		Vec3 w = Vec3.cross( new Vec3( 0.0f, 1.0f, 0.0f ), dir );
		w = w.getUnitVec3();
		setForce( Vec3.multiply( w, f ) );
		
	}

	@Override
	public void timeStep(float dT) {
		super.timeStep( dT );
		Main.camera.setPos( get_sLoc().get_rPos() );
		Main.camera.setOrientation( ori );
		Main.space.setPosition( get_sLoc().get_rPos() );
		
	}
	
	// Shooting
	
	public void shoot() { // TODO fix derpy issue where the projectiles fidget like a dumbly. I think it is a reference issue.
		if ( lastShot + 200l > System.currentTimeMillis() ) {
			return;
		} else {
			lastShot = System.currentTimeMillis();
			
		}
		
		Projectile p;
		
		Vec3 loc = Vec3.multiply( dir, getUnit().numMeters() );
		loc.add( get_sLoc().getPos() );
		
		SpatialLocation s = new SpatialLocation(
				get_sLoc().getSpace(),
				loc
				);
		
		Vec3 v = Vec3.add(
				getVel(),
				Vec3.multiply( dir, 25 * getUnit().numMeters() )
				);
		
		p = new Projectile( s, v );
		get_sLoc().getSpace().addProj( p );
		
		Main._scene.add( p.getGraphic() ); // TODO this sucks, it should be handled by something else or other.
		
	}
	
	
	// Interactable Lameness
	
	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return "Ur Lame!";

	}



}
