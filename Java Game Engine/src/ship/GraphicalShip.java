package ship;

import com.jogamp.opengl.math.Quaternion;

import main.Main;
import maths.FloatMatrix;
import maths.Vec3;
import graphicObjects.Graphic;

public class GraphicalShip extends Graphic {

	public GraphicalShip( Vec3 initPos ) {
		super( Main._gl.indexOfGraphic( ModelShip.class ), Main._tl.get( "GrayDarkGraySample.png" ), initPos);
		
		
		
	}
	
	@Override
	public FloatMatrix getModelMatrix() {
		FloatMatrix modelScale = FloatMatrix.getScaleMatrix( scl );
		Quaternion q = new Quaternion( Main.ship.getOrientation() );
		q.inverse();
		FloatMatrix modelRot   = new FloatMatrix( q.toMatrix(), 4, 4 );
		FloatMatrix modelTrans = FloatMatrix.getTranslationMatrix( pos );
		return FloatMatrix.multiplyMatrix( FloatMatrix.multiplyMatrix(modelScale, modelRot), modelTrans );
		//return modelScale;
		
	}
	
}
