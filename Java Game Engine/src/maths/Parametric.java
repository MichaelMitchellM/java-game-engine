package maths;

import java.util.Stack;

/**
 * 
 * Class for evaluating 3 Dimension single parameter equations.
 * 
 * @author Michael Mitchell
 *
 */
public class Parametric {

	private String x, y, z;

	/**
	 * Constructor
	 * 
	 * @param Post-fix equation for x.
	 * @param Post-fix equation for y.
	 * @param Post-fix equation for z.
	 */
	public Parametric( String x, String y, String z ) {
		this.x = x;
		this.y = y;
		this.z = z;

	}

	/**
	 * For evaluating x, y, and z into a Vec3 array.
	 * 
	 * @param Starting parameter (inclusive).
	 * @param Ending parameter (exclusive).
	 * @return Vec3 array of points.
	 */
	public Vec3[] evaluateToVec3( int start, int end ){
		Vec3[] _points = new Vec3[ end - start ];

		float[] _x = evaluate_eq( x, start, end );
		float[] _y = evaluate_eq( y, start, end );
		float[] _z = evaluate_eq( z, start, end );

		for ( int i = 0 ; i < end - start ; i++ ) {
			_points[ i ] = new Vec3(
					_x[ i ],
					_y[ i ],
					_z[ i ]
					);
		}
		return _points;
	}

	/**
	 * For evaluating post fix equations with one parameter.
	 * 
	 * @param Post-fix equation.
	 * @param Starting parameter (inclusive).
	 * @param Ending parameter (exclusive).
	 * @return float array of values.
	 * @throws ArithmeticException if number of operands is less than two.
	 */
	public static float[] evaluate_eq( String eq, int start, int end ){
		if ( eq == null ) return null;
		float[] _values = new float[ end - start ];
		if ( eq.equalsIgnoreCase( "0" ) ) return _values;

		int i = 0;
		for ( int t = start ; t < end ; t++ ) {
			Stack<Float> stack = new Stack<Float>();

			for ( String s : eq.split( " " ) ) {

				// Calculation
				if ( s.length() == 1 && FloatMath.Operators.isOperator(s.charAt( 0 )) ) {
					if ( stack.size() < 2 ) throw new ArithmeticException( "Need more than One operands to evaluate!" );
					stack.push(
							FloatMath.Operators.calculate(
									stack.pop(),
									stack.pop(),
									s.charAt( 0 )
									)
							);

					// Parametric
				} else if ( s.equalsIgnoreCase( "t" ) ) {
					stack.push( (float) t );

					// Function
				} else if ( FloatMath.Functions.isFunction( s ) ) {
					stack.push( FloatMath.Functions.evaluate( s , stack.pop() ) );

					// Parse Constant
				} else if ( FloatMath.Constants.isConstant( s ) ) {
					stack.push( FloatMath.Constants.getValue( s ) );

					// Parse Float
				} else {
					stack.push( Float.parseFloat( s ) );
				}
			}
			_values[ i ] = stack.pop();
			i++;
		}

		return _values;
	}

}
