package maths.exceptions;

public class FloatMathInvalidOperatorException extends RuntimeException {
	private static final long serialVersionUID = 3126892753145334800L;

	public FloatMathInvalidOperatorException( char operator ) {
		super( operator + " is not a valid Math Operator!" );
	}
	
}
