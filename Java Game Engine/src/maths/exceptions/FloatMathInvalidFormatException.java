package maths.exceptions;

public class FloatMathInvalidFormatException extends RuntimeException {
	private static final long serialVersionUID = 2563968045424186321L;

	public FloatMathInvalidFormatException( String errorMsg ) {
		super( errorMsg );
		
	}
	
}
