package maths.exceptions;

public class FloatMathUnknownFunctionException extends RuntimeException {
	private static final long serialVersionUID = -9221466181263969484L;

	public FloatMathUnknownFunctionException( String errorMsg ) {
		super( errorMsg );
	}
	
}
