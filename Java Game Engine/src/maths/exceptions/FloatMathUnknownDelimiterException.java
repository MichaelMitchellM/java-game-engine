package maths.exceptions;

public class FloatMathUnknownDelimiterException extends RuntimeException {
	private static final long serialVersionUID = 6966963262657514979L;

	public FloatMathUnknownDelimiterException( char delim ) {
		super( "Unknown Delimter: " + delim );
	}

}
