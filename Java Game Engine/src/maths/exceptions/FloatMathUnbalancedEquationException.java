package maths.exceptions;

public class FloatMathUnbalancedEquationException extends RuntimeException {
	private static final long serialVersionUID = 5127474640571211237L;

	public FloatMathUnbalancedEquationException( String equation, int location ) {
		super( "The Equation { " + equation + " } is unbalanced! Issue at character " + location );
	}
	
	public FloatMathUnbalancedEquationException( String equation, String errorMsg ) {
		super( "The Equation { " + equation + " } is unbalanced! " + errorMsg );
	}	
}
