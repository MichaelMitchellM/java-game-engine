package maths.exceptions;

public class FloatMathUnknownConstantException extends RuntimeException {
	private static final long serialVersionUID = 5327412779619483054L;

	public FloatMathUnknownConstantException( String errorMsg ) {
		super( errorMsg );
	}
	
}
