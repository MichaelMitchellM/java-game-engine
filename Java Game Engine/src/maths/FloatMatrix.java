package maths;


/**
 * @author Michael Mitchell
 * Date: 1st December, 2013
 * 
 * Purpose:
 *	Multiply two matrices of any size if they are compatible
 *		Matrices are not compatible if the num columns of the first matrix
 *		is not equal to the num rows of the second matrix
 * 
 * Info:
 * 	!!! Using Row oriented Matrices!
 * 	Matrix Object contains the list of numbers (floats in this case) and
 * 	the number of rows and number of columns.
 * 
 */

public class FloatMatrix {

	private float[] m;
	private int r, c;

	public FloatMatrix( float[] m, int r, int c ){
		this.m = m;
		this.r = r; this.c = c;

	}

	public FloatMatrix( Vec3 v ){
		this.m = new float[]{ v.x, v.y, v.z };
		this.r = 3; this.c = 1;

	}

	public int getNumRows(){
		return this.r;

	}

	public int getNumCols(){
		return this.c;

	}

	public static FloatMatrix multiplyMatrix( FloatMatrix a, FloatMatrix b ){
		if ( !(a.c == b.r) ){
			System.out.println( "Error! The Matrices given cannot be multiplied!" );
			return null;
		}
		FloatMatrix res = new FloatMatrix( new float[ a.r * b.c ], a.r, b.c );

		int i = 0;
		for ( int x = 0 ; x < res.r ; x++ ){
			for ( int y = 0 ; y < res.c ; y++ ){
				float nextEntry = 0.0f;
				int a_pos = x * a.c;
				for ( int z = 0 ; z < a.c ; z++ ){
					nextEntry += ( a.m[ (a_pos + z) ] * b.m[ ( z * b.c + y ) ] );
				}
				res.m[i] = nextEntry;
				i++;
			}
		}
		return res;

	}

	public static FloatMatrix getIdentityMatrix( int n ){
		float[] a = new float[ n*n ];
		for ( int i = 0 ; i == n*n  ; i++ ){
			if ( i % (n + 1) == 0 )
				a[ i ] = 1.0f;
			else
				a[ i ] = 0.0f;
		}
		return new FloatMatrix( a, n, n );

	}

	public static FloatMatrix getTranslationMatrix( Vec3 trans ){
		float[] a = {
				1.0f,    0.0f,    0.0f,    0.0f,
				0.0f,    1.0f,    0.0f,    0.0f,
				0.0f,    0.0f,    1.0f,    0.0f,
				trans.x, trans.y, trans.z, 1.0f,
		};
		return new FloatMatrix( a, 4, 4 );

	}

	public static FloatMatrix getScaleMatrix( Vec3 scl ){
		float[] an = {
				scl.x, 0.0f,  0.0f,  0.0f,
				0.0f,  scl.y, 0.0f,  0.0f,
				0.0f,  0.0f,  scl.z, 0.0f,
				0.0f,  0.0f,  0.0f,  1.0f,
		};
		return new FloatMatrix( an, 4, 4 );

	}

	public static FloatMatrix getScaleMatrix( float a ){
		float[] an = {
				a, 0, 0, 0,
				0, a, 0, 0,
				0, 0, a, 0,
				0, 0, 0, 1,
		};
		return new FloatMatrix( an, 4, 4 );

	}

	public static FloatMatrix getAxisRotationMatrix( float angle, Vec3 axis ){
		// Prolly Does not work :/
		float s = (float) Math.sin( angle );
		float c = (float) Math.cos( angle );
		float[] an = {
				(float) (c + axis.x*axis.x*(1-c)),
				(float) (axis.x*axis.y*(1-c) - axis.z*s),
				(float) (axis.x*axis.z*(1-c) + axis.y*s),

				(float) (axis.y*axis.x*(1-c) + axis.z*s),
				(float) (c + axis.y*axis.y*(1-c)),
				(float)	(axis.y*axis.z*(1-c) - axis.x*s),

				(float) (axis.z*axis.x*(1-c) - axis.y*s),
				(float) (axis.z*axis.y*(1-c) + axis.x*s),
				(float) (c+axis.z*axis.z*(1-c))
		};
		return new FloatMatrix( an, 3, 3 );

	}

	public static FloatMatrix getRotationMatrix( Vec3 rot ){
		return multiplyMatrix(
				multiplyMatrix(
						getRotationMatrix_x( rot.x ),
						getRotationMatrix_y( rot.y )
						),
						getRotationMatrix_z( rot.z )
				);

	}

	public static FloatMatrix getRotationMatrix_x( float theta ){
		float s, c;
		s = (float) Math.sin( theta );
		c = (float) Math.cos( theta );

		float[] an = {
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, c,    s,    0.0f,
				0.0f, -s,   c,    0.0f,
				0.0f, 0.0f, 0.0f, 1.0f,
		};
		return new FloatMatrix( an, 4, 4 );

	}

	public static FloatMatrix getRotationMatrix_y( float theta ){
		float s, c;
		s = (float) Math.sin( theta );
		c = (float) Math.cos( theta );

		float[] an = {
				c,    0.0f, -s,   0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				s,    0.0f, c,    0.0f,
				0.0f, 0.0f, 0.0f, 1.0f,
		};
		return new FloatMatrix( an, 4, 4 );

	}

	public static FloatMatrix getRotationMatrix_z( float theta ){
		float s, c;
		s = (float) Math.sin( theta );
		c = (float) Math.cos( theta );

		float[] an = {
				c,    s,    0.0f, 0.0f,
				-s,   c,    0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f,
		};
		return new FloatMatrix( an, 4, 4 );

	}

	public static FloatMatrix lookAt( Vec3 direction, Vec3 up){
		// Prolly no work, Gimbal Lock or something :/
		Vec3 s = Vec3.cross( direction, up );
		s = s.getUnitVec3();
		Vec3 u = Vec3.cross( s, direction );
		float[] an = {
				s.x, s.y, s.z, 0.0f,
				u.x, u.y, u.z, 0.0f,
				-direction.x, -direction.y, -direction.z, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
		};
		return new FloatMatrix( an, 4, 4 );

	}

	public static FloatMatrix getModel_to_WorldMatrix( Vec3 trans, Vec3 rot ){
		return multiplyMatrix( getTranslationMatrix( trans ), getRotationMatrix( rot ) );

	}

	public static FloatMatrix getPerspectiveMatrix( Double fov, float w, float h, float near, float far ){
		float asp = w/h;
		float fov_cos = (float) Math.cos( fov / 2.0d );
		float fov_sin = (float) Math.sin( fov / 2.0d );
		float fov_cot = fov_cos/fov_sin;
		float a_0  = fov_cot/asp;
		float a_3  = (far + near)/(near-far);
		float a_43 = (2.0f * far * near)/(near-far);
		float[] an = {
				a_0,  0.0f,    0.0f, 0.0f,
				0.0f, fov_cot, 0.0f, 0.0f,
				0.0f, 0.0f,    a_3,  -1.0f,
				0.0f, 0.0f,    a_43, 0.0f,
		};
		return new FloatMatrix( an, 4, 4 );

	}

	public static FloatMatrix getOrtographicMatrix( float left, float right, float bottom, float top, float near, float far ){
		float a_0 = 2.0f / (right - left);
		float a_22 = 2/(top - bottom);
		float a_33 = -2/(far - near);
		float tx = -(right + left)/(right - left);
		float ty = -(top + bottom)/(top - bottom);
		float tz = -(far + near)/(far - near);
		float[] an = {
				a_0,  0.0f, 0.0f, 0.0f,
				0.0f, a_22, 0.0f, 0.0f,
				0.0f, 0.0f, a_33, 0.0f, 
				tx,   ty,   tz,   1.0f
		};
		return new FloatMatrix( an, 4, 4 );

	}

	public float[] getArray(){
		return this.m;

	}

	public String toString(){
		String s = "| ";
		int i = 1;
		for ( float x : m ){
			if ( i % (c + 1) == 0 ){
				s += "|\n| ";
				i = 1;
			}
			s += ( x + " " );
			i++;
		}
		s += "|\nRows: ";
		return s + r + "\nCols: " + c;

	}

}
