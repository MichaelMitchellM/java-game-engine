package maths;

import java.util.Stack;

import maths.FloatMath.EquationParser;
import maths.exceptions.FloatMathUnbalancedEquationException;

public class Function {

	private String inFixEq, postFixEq;

	private int numOperations;

	private char op;

	private Object f1, f2;

	public Function( String inFix ) {

		inFix = inFix.trim();
		EquationParser.balanceCheckInfix( inFix );
		inFix = EquationParser.stripDelimiters( inFix );

		this.inFixEq = inFix;

		numOperations = FloatMath.EquationParser.getNumberOperations( inFix );

		splitFunction();

		postFixEq = parsePostFix();
		
	}

	public float evaluate(){
		// TODO Maybe allow parameters?
		float value1 = 0.0f;
		float value2 = 0.0f;

		if ( f1 instanceof Function )
			value1 = ((Function) f1).evaluate();
		else
			value1 = (float) f1;

		if ( f2 instanceof Function )
			value2 = ((Function) f2).evaluate();
		else
			value2 = (float) f2;

		System.out.println( value1 + " " + op + " " + value2 );
		
		return FloatMath.Operators.calculate( value2, value1, op );

	}
	
	public String getPostFixEquation(){
		return this.postFixEq;
		
	}
	
	public int getNumOperations(){
		return this.numOperations;
		
	}
	
	private String parsePostFix(){
		StringBuilder sb = new StringBuilder();
		if ( f1 instanceof Function )
			sb.append( ((Function) f1).parsePostFix() );
		else
			sb.append( (float) f1 );
		sb.append( " " );
		if ( f2 instanceof Function )
			sb.append( ((Function) f2).parsePostFix() );
		else
			sb.append( (float) f2 );
		sb.append( " " + op );
		return sb.toString();
		
	}
	
	private Object check( String subEq ){
		System.out.println( subEq );
		int numOps = FloatMath.EquationParser.getNumberOperations( subEq );
		if ( numOps == 0 )
			return Float.parseFloat( subEq );
		else
			return new Function( subEq );

	}

	private void splitFunction(){
		int checkNumber = 1;
		while ( checkNumber < 5 ) {
			Stack<Character> stack = new Stack<Character>();
			for ( int i = 0 ; i < inFixEq.length() ; i++ ) {
				char c = inFixEq.charAt( i );

				if ( stack.isEmpty() && i > 0 )
					switch ( checkNumber ) {
					case ( 1 ): {
						if ( c == '+' ) {
							if ( inFixEq.charAt( i-1 ) == '*'
									|| inFixEq.charAt( i-1 ) == '/'
									|| inFixEq.charAt( i-1 ) == '^' )
								continue;
							this.op = '+';
							f1 = check( inFixEq.substring( 0 , i ) );
							f2 = check( inFixEq.substring( i+1, inFixEq.length() ) );
							return;
						} else if ( c == '-' ) {
							if ( inFixEq.charAt( i-1 ) == '*'
									|| inFixEq.charAt( i-1 ) == '/'
									|| inFixEq.charAt( i-1 ) == '^' )
								continue;
							this.op = '+';
							f1 = check( inFixEq.substring( 0 , i ) );
							f2 = check( inFixEq.substring( i, inFixEq.length() ) );
							return;
						}
					} case ( 2 ): {
						if ( c == '*' ) {
							this.op = '*';
							f1 = check( inFixEq.substring( 0 , i) );
							f2 = check( inFixEq.substring( i+1, inFixEq.length() ) );
							return;
						} else if ( c == '/' ) {
							this.op = '/';
							f1 = check( inFixEq.substring( 0 , i ) );
							f2 = check( inFixEq.substring( i+1, inFixEq.length() ) );
							return;
						}
					} case ( 3 ): {
						if ( c == '^' ) {
							this.op = '^';
							f1 = check( inFixEq.substring( 0 , i ) );
							f2 = check( inFixEq.substring( i+1, inFixEq.length() ) );
						}
					}
					}

				if ( EquationParser.isDelimiter( c ) != -1 ) {
					if ( stack.isEmpty() ) {
						if ( EquationParser.isStartingDelimiter( c ) )
							stack.push( c );
						else throw new FloatMathUnbalancedEquationException( inFixEq, i );

					} else if ( EquationParser.isStartingDelimiter( c ) ) {
						stack.push( c );
					} else {
						if ( c == EquationParser.getReciprocalDelimiter( stack.peek() ) )
							stack.pop();
						else throw new FloatMathUnbalancedEquationException( inFixEq,  i );

					}
				}
			}
			checkNumber++;
		}
	}

}