package maths;

/**
 * 3D Float Vector class which also have a complement of math functions.
 * 
 * @author Michael Mitchell
 *
 */
public class Vec3 {


	public float x, y, z;

	/**
	 * Constructor
	 * 
	 * @param x component.
	 * @param y component.
	 * @param z component.
	 */
	public Vec3( float x, float y, float z ){
		this.x = x; this.y = y; this.z = z;
	}

	/**
	 * Constructor
	 * sets x, y, z to the single parameter.
	 * 
	 * @param x, y, and z component.
	 */
	public Vec3( float s ){
		this.x = s; this.y = s; this.z = s;
	}

	/**
	 * Constructor
	 * Sets a Vec2 into a Vec3 with a given z value for the Vec3.
	 * 
	 * @param v
	 * @param z
	 */
	public Vec3( Vec2 v, float z ){
		this.x = v.x;
		this.y = v.y;
		this.z = z;

	}

	/**
	 * Constructor
	 * Sets a Vec2 into a Vec3, sets the Vec3's z value to 0.0f
	 * 
	 * @param v
	 */
	public Vec3( Vec2 v ){
		this.x = v.x;
		this.y = v.y;
		this.z = 0.0f;

	}

	/**
	 * Constructor
	 * Sets x, y, and z to Zero.
	 * 
	 */
	public Vec3(){
		this.x = 0.0f; this.y = 0.0f; this.z = 0.0f;

	}

	public Vec3( FloatMatrix m ){
		if ( m.getNumRows() == 3 && m.getNumCols() == 1 ) {
			this.x = m.getArray()[0];
			this.y = m.getArray()[1];
			this.z = m.getArray()[2];
		} else {
			System.out.println( "Cannot cast FloatMatrix to Vec3!" ); // TODO error thing here pls.

		}
	}

	// End of Constructors.

	/**
	 * Gets unit vector.
	 * 
	 * @return unit vector of the Vec3.
	 */
	public Vec3 getUnitVec3(){
		float mag = getMagnitude();
		return Vec3.divide( this, mag );

	}

	/**
	 * Gets magnitude.
	 * 
	 * @return float Magnitude of the Vec3.
	 */
	public float getMagnitude(){
		return (float) Math.sqrt( (x*x) + (y*y) + (z*z) );

	}

	/**
	 * Gets Theta
	 * 
	 * @return radians of the angle between the Vec3 and the positive X-axis.
	 */
	public float getTheta(){
		if ( x == 0 )
			if ( y > 0.0f )
				return (float) (Math.PI / 2.0d);
			else
				return (float) -(Math.PI / 2.0d);
		double theta = Math.atan( y / x );
		if ( x < 0.0f ) {
			// This does not take into account if z = 0.0f !!!
			if ( z < 0.0f )
				theta -= Math.PI;
			else
				theta += Math.PI;
		}
		return (float) theta;
	}

	/**
	 * Gets Phi
	 * 
	 * @return radians of the angle between the Vector and the positive Z-axis.
	 */
	public float getPhi(){
		if ( x == 0.0f && y == 0.0f && z == 0.0f )
			return (float) (Math.PI / 2.0d);
		return (float) (Math.acos( z / (Math.sqrt( x*x + y*y + z*z )) ));
	}

	// Vector Math:

	/**
	 * Adds a Vec3 to the current Vec3.
	 * 
	 * @param Vec3 to add to Vec3 Object.
	 */
	public void add( Vec3 a ){
		this.x += a.x;
		this.y += a.y;
		this.z += a.z;

	}

	/**
	 * Adds two Vec3s and returns the resulting Vec3.
	 * 
	 * @param Vec3 operand 1.
	 * @param Vec3 operand 2.
	 * @return Vec3 from adding two Vec3s.
	 */
	public static Vec3 add( Vec3 a, Vec3 b ){
		return new Vec3(
				a.x + b.x,
				a.y + b.y,
				a.z + b.z
				);

	}

	/**
	 * Subtracts a Vec3 from the current Vec3.
	 * 
	 * @param Vec3 to subtract to the Vec3 Object.
	 */
	public void subtract( Vec3 a ){
		this.x -= a.x;
		this.y -= a.y;
		this.z -= a.z;

	}

	/**
	 * Subtracts two Vec3s and return the resulting Vec3.
	 * 
	 * @param Vec3 operand 1.
	 * @param Vec3 operand 2.
	 * @return Vec3 from subtracting two Vec3s.
	 */
	public static Vec3 subtract( Vec3 a, Vec3 b ){
		// a = end, b = start.
		return new Vec3(
				a.x - b.x,
				a.y - b.y,
				a.z - b.z
				);

	}

	/**
	 * Multiplies a scalar to the current Vec3.
	 * 
	 * @param scalar
	 */
	public void multiply( float scalar ){
		this.x *= scalar;
		this.y *= scalar;
		this.z *= scalar;

	}

	/**
	 * Multiplies a scalar to a Vec3 and return the resulting Vec3.
	 * 
	 * @param Vec3
	 * @param scalar
	 * @return resulting Vec3 from multiplying a Vec3 by a scalar.
	 */
	public static Vec3 multiply( Vec3 a, float scalar ){
		return new Vec3(
				a.x * scalar,
				a.y * scalar,
				a.z * scalar
				);

	}

	/**
	 * Divides the current Vec3 by a scalar.
	 * 
	 * @param scalar
	 */
	public void divide( float scalar ){
		this.x /= scalar;
		this.y /= scalar;
		this.z /= scalar;

	}

	/**
	 * Divides a Vec3 by a scalar and returns the resulting Vec3.
	 * 
	 * @param Vec3
	 * @param scalar
	 * @return resulting Vec3 from dividing a Vec3 by a scalar.
	 */
	public static Vec3 divide( Vec3 a, float scalar ){
		return new Vec3(
				a.x / scalar,
				a.y / scalar,
				a.z / scalar
				);

	}

	/**
	 * Calculates the Dot Product of two Vec3's.
	 * Order of entered Vec3's do no matter.
	 * 
	 * @param a Vec3
	 * @param b Vec3
	 * @return float dot product of a and b.
	 */
	public static float dot( Vec3 a, Vec3 b ){
		return a.x*b.x + a.y*b.y + a.z*b.z;

	}

	public static Vec3 cross( Vec3 a, Vec3 b ){
		return new Vec3( a.y*b.z - b.y*a.z, a.z*b.x - b.z*a.x, a.x*b.y - b.x*a.y );

	}

	// Higher Level Functions:

	public static Vec3 proj( Vec3 u, Vec3 v ){
		return Vec3.multiply( v, Vec3.dot( u, v )/(v.getMagnitude() * v.getMagnitude()) );

	}

	public static Vec3 compOrthogonal( Vec3 u, Vec3 v ){
		return Vec3.subtract( u, Vec3.proj( u, v ) );

	}

	@Override
	public String toString() {
		return ""
				+   "x : " + x 
				+ "\ny : " + y
				+ "\nz : " + z;
	}

}
