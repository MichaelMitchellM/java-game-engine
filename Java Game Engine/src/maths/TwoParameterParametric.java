package maths;

import java.util.Stack;

/**
 * 
 * Class for evaluating post-fix equations that have two parameters.
 * 
 * @author Michael Mitchell
 * @see Parametric
 *
 */
public class TwoParameterParametric {

	private String x, y, z;
	
	/**
	 * Constructor
	 * 
	 * @param Post-fix equation for x.
	 * @param Post-fix equation for y.
	 * @param Post-fix equation for z.
	 */
	public TwoParameterParametric( String x, String y, String z ) {
		this.x = x;
		this.y = y;
		this.z = z;
		
	}

	/**
	 * For evaluating x, y, and z into a Vec3 array.
	 * 
	 * @param First Starting Parameter (inclusive).
	 * @param First Ending Parameter (exclusive).
	 * @param Second Starting Parameter (inclusive).
	 * @param Second Ending Parameter (exclusive).
	 * @return Vec3 array of points.
	 */
	public Vec3[] evaluateToVec3( int tStart, int tEnd, int uStart, int uEnd ){
		Vec3[] _points = new Vec3[ (tEnd - tStart) * (uEnd - uStart) ];

		float[] _x = evaluate_eq( x, tStart, tEnd, uStart, uEnd );
		float[] _y = evaluate_eq( y, tStart, tEnd, uStart, uEnd );
		float[] _z = evaluate_eq( z, tStart, tEnd, uStart, uEnd );

		for ( int i = 0 ; i < (tEnd - tStart) * (uEnd - uStart) ; i++ ) {
			_points[ i ] = new Vec3(
					_x[ i ],
					_y[ i ],
					_z[ i ]
					);
		}
		
		return _points;
	}
	
	/**
	 * For evaluating post fix equations with two parameters.
	 * 
	 * @param Post-fix equation.
	 * @param First Starting Parameter (inclusive).
	 * @param First Ending Parameter (exclusive).
	 * @param Second Starting Parameter (inclusive).
	 * @param Second Ending Parameter (exclusive).
	 * @return float array of values.
	 * @throws ArithmeticException if number of operands is less than two.
	 */
	public float[] evaluate_eq( String eq, int tStart, int tEnd, int uStart, int uEnd ){
		if ( eq == null ) return null;
		float[] _values = new float[ (tEnd - tStart) * (uEnd - uStart) ];
		if ( eq.equalsIgnoreCase( "0" ) ) return _values;
		
		int i = 0;
		for ( int t = tStart ; t < tEnd ; t++ ) {
			for ( int u = uStart ; u < uEnd ; u++ ) {
				Stack<Float> stack = new Stack<Float>();

				for ( String s : eq.split( " " ) ) {

					// Calculation
					if ( s.length() == 1 && FloatMath.Operators.isOperator(s.charAt( 0 )) ) {
						if ( stack.size() < 2 ) throw new ArithmeticException( "Need more than One operands to evaluate!" );
						stack.push(
								FloatMath.Operators.calculate(
										stack.pop(),
										stack.pop(),
										s.charAt( 0 )
										)
								);

						// Parametric t
					} else if ( s.equalsIgnoreCase( "t" ) ) {
						stack.push( (float) t );
						
						// Parametric u
					} else if ( s.equalsIgnoreCase( "u" ) ) {
						stack.push( (float) u );
						
						// Function
					} else if ( FloatMath.Functions.isFunction( s ) ) {
						stack.push( FloatMath.Functions.evaluate( s , stack.pop() ) );

						// Parse Constant
					} else if ( FloatMath.Constants.isConstant( s ) ) {
						stack.push( FloatMath.Constants.getValue( s ) );

						// Parse Float
					} else {
						stack.push( Float.parseFloat( s ) );
					}
				}
				_values[ i ] = stack.pop();
				i++;
			}
		}
		return _values;
		
	}
	
}
