package maths;

public class Vec2 {

	public float x;
	public float y;
	
	public Vec2( float x, float y ) {
		this.x = x; this.y = y;
		
	}
	
	public Vec2( float s ){
		this.x = s; this.y = s;
		
	}
	
	public Vec2() {
		this.x = 0.0f; this.y = 0.0f;
		
	}
	
	// End of Constructors.
	
		/**
		 * Gets unit vector.
		 * 
		 * @return unit vector of the Vec2.
		 */
		public Vec2 getUnitVec2(){
			float mag = getMagnitude();
			return Vec2.divide( this, mag );
			
		}

		/**
		 * Gets magnitude.
		 * 
		 * @return float Magnitude of the Vec2.
		 */
		public float getMagnitude(){
			return (float) Math.sqrt( (x*x) + (y*y) );
			
		}
		
		// Vector Math:

		/**
		 * Adds a Vec2 to the current Vec2.
		 * 
		 * @param Vec2 to add to Vec2 Object.
		 */
		public void add( Vec2 a ){
			this.x += a.x;
			this.y += a.y;
			
		}
		
		/**
		 * Adds two Vec2s and returns the resulting Vec2.
		 * 
		 * @param Vec2 operand 1.
		 * @param Vec2 operand 2.
		 * @return Vec2 from adding two Vec2s.
		 */
		public static Vec2 add( Vec2 a, Vec2 b ){
			return new Vec2(
					a.x + b.x,
					a.y + b.y
					);
			
		}

		/**
		 * Subtracts a Vec2 from the current Vec2.
		 * 
		 * @param Vec2 to subtract to the Vec2 Object.
		 */
		public void subtract( Vec2 a ){
			this.x -= a.x;
			this.y -= a.y;
			
		}
		
		/**
		 * Subtracts two Vec2s and return the resulting Vec2.
		 * 
		 * @param Vec2 operand 1.
		 * @param Vec2 operand 2.
		 * @return Vec2 from subtracting two Vec2s.
		 */
		public static Vec2 subtract( Vec2 a, Vec2 b ){
			// a = end, b = start.
			return new Vec2(
					a.x - b.x,
					a.y - b.y
					);
			
		}

		/**
		 * Multiplies a scalar to the current Vec2.
		 * 
		 * @param scalar
		 */
		public void multiply( float scalar ){
			this.x *= scalar;
			this.y *= scalar;
			
		}
		
		/**
		 * Multiplies a scalar to a Vec2 and return the resulting Vec2.
		 * 
		 * @param Vec2
		 * @param scalar
		 * @return resulting Vec2 from multiplying a Vec2 by a scalar.
		 */
		public static Vec2 multiply( Vec2 a, float scalar ){
			return new Vec2(
					a.x * scalar,
					a.y * scalar
					);
			
		}

		/**
		 * Divides the current Vec2 by a scalar.
		 * 
		 * @param scalar
		 */
		public void divide( float scalar ){
			this.x /= scalar;
			this.y /= scalar;
			
		}
		
		/**
		 * Divides a Vec2 by a scalar and returns the resulting Vec2.
		 * 
		 * @param Vec2
		 * @param scalar
		 * @return resulting Vec2 from dividing a Vec2 by a scalar.
		 */
		public static Vec2 divide( Vec2 a, float scalar ){
			return new Vec2(
					a.x / scalar,
					a.y / scalar
					);
			
		}
		
		/**
		 * Calculates the Dot Product of two Vec2's.
		 * Order of entered Vec2's do no matter.
		 * 
		 * @param a Vec2
		 * @param b Vec2
		 * @return float dot product of a and b.
		 */
		public static float dot( Vec2 a, Vec2 b ){
			return a.x*b.x + a.y*b.y;
			
		}
	
}
