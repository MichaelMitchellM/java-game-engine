package maths;

import java.util.Stack;

import maths.exceptions.FloatMathInvalidOperatorException;
import maths.exceptions.FloatMathUnbalancedEquationException;
import maths.exceptions.FloatMathUnknownConstantException;
import maths.exceptions.FloatMathUnknownDelimiterException;
import maths.exceptions.FloatMathUnknownFunctionException;

/**
 * This class is the main class for Math related functions, 
 * its name is up for change, but is currently as is because it works in floats.
 * 
 * @author Michael Mitchell
 * 
 */
public class FloatMath {

	/**
	 * 
	 * Static inner class for holding Mathematical constants
	 * and returning their values.
	 * 
	 * @author Michael Mitchell
	 *
	 */
	public static class Constants {

		public static final String[] _constants = {
			"pi", "e"
		};

		/**
		 * For getting the value Mathematical constants.
		 * 
		 * @param the name of the constant.
		 * @return value of the constant.
		 * @throws FloatMathUnkownConstantException if the constant entered is not in the list of constants.
		 */
		public static float getValue( String constant ){

			switch ( constant ) {
			case "pi":
				return (float) Math.PI;
			case "e":
				return (float) Math.E;
			default:
				throw new FloatMathUnknownConstantException( "Unknown constant: " + constant );
			}
		}

		/**
		 * For checking if a value is in the list of constants.
		 * 
		 * @param the name of the constant
		 * @return true if the String is found in the list of constants, else false.
		 */
		public static boolean isConstant( String cons ){
			for ( String c : _constants )
				if ( cons.equals( c ) )
					return true;
			return false;
		}

	}

	/**
	 * 
	 * Static inner class for holding Mathematical operators
	 * and evaluating operands with them.
	 * 
	 * @author Michael Mitchell
	 *
	 */
	public static class Operators {

		public static final char[] _ops = {
			'+', '-', '*', '/', // Basic Operators
			'^'
		};

		/**
		 * Calculates two operands with an operator to get a value.
		 * 
		 * @param Second operand
		 * @param First operand
		 * @param operator
		 * @return value of computation.
		 * @throws FloatMathInvalidOperatorException if the operator entered is not in the list of operators.
		 */
		public static float calculate( float f2, float f1, char operator ) {

			switch ( operator ) {
			case '+':
				return f1 + f2;
			case '-':
				return f1 - f2;
			case '*':
				return f1 * f2;
			case '/':
				if ( f2 == 0.0f ) throw new ArithmeticException( "Divide by Zero" );
				return f1 / f2;
			case '^':
				return (float) Math.pow( f1, f2 );
			default:
				throw new FloatMathInvalidOperatorException( operator );
			}
		}

		/**
		 * For checking if a character is in the list of operators.
		 * 
		 * @param operator
		 * @return true if the character is in the list of operators.
		 */
		public static boolean isOperator( char operator ){
			for ( char op : _ops )
				if ( op == operator )
					return true;
			return false;
		}

	}

	/**
	 * 
	 * Static inner class for holding Mathematical functions
	 * and evaluating numbers with the functions.
	 * 
	 * @author Michael Mitchell
	 *
	 */
	public static class Functions {

		public static final String[] _funcs = {
			"sin", "cos", "tan",
			"arcsin", "arccos", "arctan"
		};

		/**
		 * For evaluating functions.
		 * 
		 * @param function name.
		 * @param number to be evaluated.
		 * @return evaluated number.
		 * @throws ArithmeticException if the input value is out of the domain of the function.
		 * @throws FloatMathUnknownFunctionException if the entered function is not in the list of functions.
		 */
		public static float evaluate( String func, float val ){

			switch ( func ) {
			case ( "sin" ):
				return (float) Math.sin( val );
			case ( "cos" ):
				return (float) Math.cos( val );
			case ( "tan" ):
				return (float) Math.tan( val );
			case ( "arcsin" ):
				if ( Math.abs( val ) > 1.0d ) throw new ArithmeticException( "Arcsin cannot take a number who's absolute value is greater than Zero!" );
			return (float) Math.asin( val );
			case ( "arccos" ):
				if ( Math.abs( val ) > 1.0d ) throw new ArithmeticException( "Arccos cannot take a number who's absolute value is greater than Zero!" );
			return (float) Math.acos( val );
			case ( "arctan" ):
				return (float) Math.atan( val );
			default:
				throw new FloatMathUnknownFunctionException( "Unknown Function: " + func );
			}
		}

		/**
		 * For checking if the entered string is in the list of functions.
		 * 
		 * @param function name.
		 * @return true if String is in list, else false.
		 */
		public static boolean isFunction( String func ){
			for ( String f : _funcs )
				if ( func.equalsIgnoreCase( f ) )
					return true;
			return false;
		}
	}

	/**
	 * 
	 * Static inner class for evaluating post-fix equations.
	 * 
	 * @author Michael Mitchell
	 *
	 */
	public static class PostFixEvaluator {

		/**
		 * Evaluates a post-fix equation from a string.
		 * 
		 * @param post-fix equation.
		 * @return evaluated result.
		 * @throws ArithmeticException if less than two operands are given to evaluate.
		 */
		public static float evaluate( String eq ){
			Stack<Float> stack = new Stack<Float>();

			for ( String s : eq.split( " " ) ) {

				// Calculation
				if ( s.length() == 1 && FloatMath.Operators.isOperator(s.charAt( 0 )) ) {
					if ( stack.size() < 2 ) throw new ArithmeticException( "Need more than One operands to evaluate!" );
					stack.push(
							FloatMath.Operators.calculate(
									stack.pop(),
									stack.pop(),
									s.charAt( 0 )
									)
							);

					// Function
				} else if ( FloatMath.Functions.isFunction( s ) ) {
					stack.push( FloatMath.Functions.evaluate( s , stack.pop() ) );

					// Parse Constant
				} else if ( FloatMath.Constants.isConstant( s ) ) {
					stack.push( FloatMath.Constants.getValue( s ) );

					// Parse Float
				} else {
					stack.push( Float.parseFloat( s ) );
				}
			}

			return stack.pop();
		}

	}

	public static class EquationParser {

		public static final char[] _delimiters = {
			'(', ')',
			'[', ']',
			'{', '}'
		};

		public static String parsePostFix( String inFix ){
			return (new Function( inFix )).getPostFixEquation();
			
		}

		public static String parseInfix( String postFix ){



			return null;
		}

		public static int getNumberOperations( String inFix ){
			// TODO this needs work.
			int numOperations = 0;
			for ( int i = 1 ; i < inFix.length() ; i++ ) {
				char c = inFix.charAt( i );
				if ( Operators.isOperator( c ) )
					numOperations++;
			}
			return numOperations;
		}
		
		public static void balanceCheckInfix( String inFix ){
			Stack<Character> stack = new Stack<Character>();
			for ( int i = 0 ; i < inFix.length() ; i++ ) {
				char c = inFix.charAt( i );

				if ( isDelimiter( c ) != -1 ) {
					if ( stack.isEmpty() ) {
						if ( isStartingDelimiter( c ) )
							stack.push( c );
						else throw new FloatMathUnbalancedEquationException( inFix, i );

					} else if ( isStartingDelimiter( c ) ) {
						stack.push( c );
					} else {
						if ( c == getReciprocalDelimiter( stack.peek() ) )
							stack.pop();
						else throw new FloatMathUnbalancedEquationException( inFix,  i );

					}
				}
			}
			if ( !stack.isEmpty() ) throw new FloatMathUnbalancedEquationException( inFix, "Equation has not enough closing delimiters!") ;
		}

		public static String stripDelimiters( String inFix ){
			Stack<Character> stack = new Stack<Character>();
			
			for ( int i = 0 ; i < inFix.length()-1 ; i++ ) {
				char c = inFix.charAt( i );

				if ( isDelimiter( c ) != -1 ) {
					if ( stack.isEmpty() ) {
						if ( isStartingDelimiter( c ) )
							stack.push( c );
						else throw new FloatMathUnbalancedEquationException( inFix, i );

					} else if ( isStartingDelimiter( c ) ) {
						stack.push( c );
					} else {
						if ( c == getReciprocalDelimiter( stack.peek() ) )
							stack.pop();
						else throw new FloatMathUnbalancedEquationException( inFix,  i );

					}
				}
				
				if ( stack.isEmpty() ) return inFix;
				
			}
			if ( stack.isEmpty() ) {
				return inFix;
			} else if ( stack.size() > 1 ) {
				throw new FloatMathUnbalancedEquationException( inFix, "Equation has to many delimiters!" );
			} else if ( inFix.charAt( inFix.length()-1 ) == getReciprocalDelimiter( stack.peek() ) ) {
				return inFix.substring( 1, inFix.length()-1 );
			} else throw new FloatMathUnbalancedEquationException( inFix, inFix.length()-1 );
		}

		public static int isDelimiter( char delim ){
			int i = 0;
			for ( char d : _delimiters ) {
				if ( d == delim )
					return i;
				i++;
			}

			return -1;
		}

		public static boolean isStartingDelimiter( char delim ){
			int delimIndex = isDelimiter( delim );
			if ( delimIndex != -1 ) {
				if ( delimIndex % 2 == 0 ) return true;
				else return false;

			} else throw new FloatMathUnknownDelimiterException( delim );
		}

		public static char getReciprocalDelimiter( char delim ){
			int delimIndex = isDelimiter( delim );
			if ( delimIndex != -1 ) {
				if ( isStartingDelimiter( delim ) )
					return _delimiters[ delimIndex + 1 ];
				else
					return _delimiters[ delimIndex - 1 ];

			} else throw new FloatMathUnknownDelimiterException( delim );
		}

	}
}

