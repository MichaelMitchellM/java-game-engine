package hud;

import icon.Icon;

import java.util.ArrayList;

import javax.media.opengl.GL3;
import javax.media.opengl.glu.GLU;

import main.Main;
import maths.Vec3;
import physicalObjects.SpatialObject;

public class HUD {

	private ArrayList<SpatialObject> _sos = new ArrayList<SpatialObject>();

	public HUD() {



	}

	public void update( GL3 gl ) {

		GLU glu = new GLU();

		int i = 0;
		for ( SpatialObject so : _sos ) {
			// TODO make so icons from things behind camera are not tracked.

			float[] point     = new float[ 3 ];
			float[] modelview = Main.camera.getCameraMatrix().getArray();
			float[] proj      = Main.proj.getArray();
			int[]   viewport  = { -(int) (Main.width/2.0f),  -(int) (Main.height/2.0f), (int) Main.width, (int) Main.height };

			glu.gluProject(
					so.get_sLoc().get_rPos().x,
					so.get_sLoc().get_rPos().y,
					so.get_sLoc().get_rPos().z,
					modelview, 0,
					proj,      0,
					viewport,  0,
					point,     0
					);

			Main._icons.get( i ).getIconImage().setPosition( new Vec3( point[ 0 ], point[ 1 ], 0.0f ) );

			i++;
		}


	}

	public void track( ArrayList<SpatialObject> _sos ) {
		Main._icons.clear();

		for ( int i = 0 ; i < _sos.size() ; i++ ) {
			if ( _sos.get( i ).getIconImageType() != null ) {
				Main._icons.add( new Icon( _sos.get( i ) ) );
				this._sos.add( _sos.get( i ) );

			}
		}
	}

}
