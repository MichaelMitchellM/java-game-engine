package main;

import graphicObjects.Graphic;

import java.util.ArrayList;

import com.jogamp.opengl.math.Quaternion;

import maths.FloatMatrix;
import maths.Vec3;

public class Camera {

	private Vec3 pos = new Vec3();

	private Quaternion ori = new Quaternion( new float[]{ 0.0f, 1.0f, 0.0f }, 0.0f );

	public Camera() {
	}

	public void view( ArrayList<Graphic> _gs ) {
		Main._scene.clear();
		for ( int i = 0 ; i < _gs.size() ; i++ )
			Main._scene.add( _gs.get( i ) );

	}

	public FloatMatrix getCameraMatrix() {
		// TODO this needs to not suck!
		return FloatMatrix.multiplyMatrix(
				FloatMatrix.getTranslationMatrix( Vec3.multiply( pos, -1.0f ) ),
				new FloatMatrix( ori.toMatrix(), 4, 4 )
				);
	}

	/* TODO fix, currently used direction and stuff, there is no more direction.
	public void lookAt( Vec3 point ){
		point = point.getUnitVec3();
		this.direction = Vec3.subtract( point, this.pos );
		//System.out.println( direction );
		rot = new Vec3( getRot_x(), (float) (Math.PI / 2.0d) - getRot_y(), 0.0f );
		//rot = new Vec3( 0.0f, (float) (-Math.PI/2.0d), 0.0f );
		//System.out.println( rot );

	}
	 */

	public void setOrientation( Quaternion ori ) {
		this.ori = ori;

	}

	public Quaternion getOrientation() {
		return this.ori;

	}

	public void setPos( Vec3 pos ) {
		this.pos = pos;

	}

	public void addPos( Vec3 pos ) {
		this.pos.add( pos );

	}

	public Vec3 getPos() {
		return Vec3.multiply( this.pos , -1.0f );

	}

}
