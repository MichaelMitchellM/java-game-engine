package main;

import graphicObjects.Graphic;
import graphicObjects.graphic2D.GraphicContainer;
import gui.GuiObject;
import icon.Icon;

import java.io.File;
import java.nio.IntBuffer;

import javax.media.opengl.GL3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import maths.FloatMatrix;
import maths.Vec3;

/**
 * 
 * @author Michael Mitchell
 * 
 */

public class MyGLEventListener implements GLEventListener {

	private VeryGui gui;

	public float width = 0.0f, height = 0.0f;
	public double fov = Math.PI / 6.0d;

	private IntBuffer vbos, tbos;

	private String vertexShader, fragmentShader;
	private int shaderProgram;

	private int modelMatrixLoc, worldMatrixLoc, viewMatrixLoc;
	private int textureUnLoc;

	// Matrices
	private FloatMatrix ortho;
	private FloatMatrix modelToWorld;

	public MyGLEventListener( VeryGui gui ){

		this.gui = gui;

		// Shaders!
		vertexShader =	""
				+ "#version 330\r\n"
				+ ""
				+ "in vec3 position;"
				+ "in vec2 vertUV;"
				+ ""
				+ "uniform mat4 modelMatrix;"
				+ "uniform mat4 worldMatrix;"
				+ "uniform mat4 viewMatrix;"
				+ ""
				+ "out vec2 UV;"
				+ ""
				+ "void main()"
				+ "{"
				+ "    vec4 v = vec4( position, 1.0 );"
				+ "    gl_Position = viewMatrix * worldMatrix * modelMatrix * v;"
				+ "    UV = vertUV;"
				+ "}"
				+ "";
		
		fragmentShader = ""
				+ "#version 330\r\n"
				+ ""
				+ "in vec2 UV;"
				+ ""
				+ "uniform sampler2D myTexture;"
				+ ""
				+ "out vec4 endColor;"
				+ ""
				+ ""
				+ "void main()"
				+ "{"
				+ "    endColor = texture( myTexture, UV );"
				+ "}"
				+ "";

	}

	@Override
	public void display( GLAutoDrawable draw ) {

		GL3 gl = draw.getGL().getGL3();

		gl.glClearColor( 0.5f, 0.5f, 0.5f, 1.0f );
		gl.glClear( GL3.GL_COLOR_BUFFER_BIT | GL3.GL_DEPTH_BUFFER_BIT );

		gl.glUseProgram( shaderProgram );

		// Matrices
		// Model

		gl.glPointSize( 3.0f );

		// _____ SHARED MATRICES _____

		try {
			modelToWorld = Main.camera.getCameraMatrix();
			
		} catch( Exception e ) {
			// nothing to see here.
		}

		//  --------- SCENE ----------

		gl.glEnable( GL3.GL_DEPTH_TEST );

		FloatMatrix proj = FloatMatrix.getPerspectiveMatrix( this.fov, this.width, this.height, 0.1f, 400.0f );
		Main.proj = proj;

		for ( int i = 0 ; i < Main._scene.size() ; i++ ){

			gl.glUniformMatrix4fv( modelMatrixLoc, 1, false, Main._scene.get( i ).getModelMatrix().getArray(), 0 );
			gl.glUniformMatrix4fv( worldMatrixLoc, 1, false, modelToWorld.getArray(), 0 );
			gl.glUniformMatrix4fv( viewMatrixLoc , 1, false, proj.getArray(), 0 );
			// Draw!
			Main._scene.get( i ).draw( gl, textureUnLoc, vbos, tbos );

		}

		gl.glDisable( GL3.GL_DEPTH_TEST );

		// --------- END SCENE ----------

		// ---------- GUI ----------

		gl.glClear( GL3.GL_DEPTH_BUFFER_BIT );

		gl.glEnable( GL3.GL_BLEND );
		gl.glBlendFunc( GL3.GL_SRC_ALPHA, GL3.GL_ONE_MINUS_SRC_ALPHA );

		modelToWorld = FloatMatrix.getTranslationMatrix( new Vec3( 0.0f, 0.0f, -1.0f ) ); // Where Camera Matrix Would be.

		ortho = FloatMatrix.getOrtographicMatrix( -this.width/2.0f, this.width/2.0f, -this.height/2.0f, this.height/2.0f, 1.0f, 1.1f );

		// ---------- HUD ----------

		for ( Icon i : Main._icons ) {
			
			gl.glUniformMatrix4fv( modelMatrixLoc, 1, false, i.getIconImage().getModelMatrix().getArray(), 0 );
			gl.glUniformMatrix4fv( worldMatrixLoc, 1, false, modelToWorld.getArray(), 0 );
			gl.glUniformMatrix4fv( viewMatrixLoc , 1, false, ortho.getArray(), 0 );
			
			i.getIconImage().draw( gl, textureUnLoc, vbos, tbos );

		}

		// Windows 
		for ( GraphicContainer gc : Main._windows ) {
			drawGraphicContainer(gl, gc);

		}

		Main.hud.update( gl );

		// ----- END HUD ------

		// ----- Do GUI ------

		for ( int i = Main.gui.getGuiObjects().size()-1 ; i >= 0 ; i-- ) {
			GuiObject go = Main.gui.getGuiObjects().get( i );
			if ( go.getGraphic() instanceof GraphicContainer ) {
				drawGraphicContainer( gl, (GraphicContainer) go.getGraphic() );
			} else {
				
				gl.glUniformMatrix4fv( modelMatrixLoc, 1, false, go.getGraphic().getModelMatrix().getArray(), 0 );
				gl.glUniformMatrix4fv( worldMatrixLoc, 1, false, modelToWorld.getArray(), 0 );
				gl.glUniformMatrix4fv( viewMatrixLoc , 1, false, ortho.getArray(), 0 );
				
				go.getGraphic().draw( gl, textureUnLoc, vbos, tbos );

			}
		}

		gl.glDisable( GL3.GL_BLEND );

		// ----- END GUI ------
		
		// FPS counter
		Main.frc.addFrame();
		
	}

	private void drawGraphicContainer( GL3 gl, GraphicContainer gc ){
		/*
		 * TODO change maybe?
		 * Not sure how much I like this function, 
		 * it does serve a purpose,
		 * but it may have issues with overlapping graphics in the wrong way.
		 * But I don't know, it works for now.
		 * 
		 */
		
		FloatMatrix modelFinal = gc.getModelMatrix();
		
		gl.glUniformMatrix4fv( modelMatrixLoc, 1, false, modelFinal.getArray(), 0 );
		gl.glUniformMatrix4fv( worldMatrixLoc, 1, false, modelToWorld.getArray(), 0 );
		gl.glUniformMatrix4fv( viewMatrixLoc , 1, false, ortho.getArray(), 0 );
		
		gc.draw( gl, textureUnLoc, vbos, tbos );

		for ( Graphic g : gc.getContainedGraphics() ) {
			if ( g instanceof GraphicContainer ) {
				drawGraphicContainer( gl, (GraphicContainer) g );

			} else {
				
				modelFinal = g.getModelMatrix();
				
				gl.glUniformMatrix4fv( modelMatrixLoc, 1, false, modelFinal.getArray(), 0 );
				gl.glUniformMatrix4fv( worldMatrixLoc, 1, false, modelToWorld.getArray(), 0 );
				gl.glUniformMatrix4fv( viewMatrixLoc , 1, false, ortho.getArray(), 0 );
				
				g.draw( gl, textureUnLoc, vbos, tbos );

			}
		}

	}

	@Override
	public void dispose( GLAutoDrawable gl ) {
		System.out.println( "Program Closing" );
		System.exit( 0 );
	}

	@Override
	public void init( GLAutoDrawable draw ) {
		this.width  = (float) this.gui.glCanvas.getWidth();
		this.height = (float) this.gui.glCanvas.getHeight();

		GL3 gl = draw.getGL().getGL3();

		long openGLStart = System.currentTimeMillis();
		System.out.println( "_____OpenGL Initializing_____" );

		long modelStart = System.currentTimeMillis();
		System.out.println( "----- Loading Models -----\n" );

		vbos = IntBuffer.allocate( Main._gl.size() );
		tbos = IntBuffer.allocate( Main._gl.size() );

		gl.glGenBuffers( Main._gl.size(), vbos );
		gl.glGenBuffers( Main._gl.size(), tbos );
		for ( int i = 0 ; i < Main._gl.size() ; i++ ) {

			gl.glBindBuffer( GL3.GL_ARRAY_BUFFER, vbos.get( i ) );
			gl.glBufferData(
					GL3.GL_ARRAY_BUFFER,
					Main._gl.get( i ).getVertBuffer().capacity() * 4,
					Main._gl.get( i ).getVertBuffer(),
					GL3.GL_STATIC_DRAW
					);

			gl.glBindBuffer( GL3.GL_ARRAY_BUFFER, tbos.get( i ) );
			gl.glBufferData(
					GL3.GL_ARRAY_BUFFER,
					Main._gl.get( i ).getTexBuffer().capacity() * 4,
					Main._gl.get( i ).getTexBuffer(),
					GL3.GL_STATIC_DRAW
					);

		}

		System.out.println( "----- Models Loaded! ----- [time : "
				+ (System.currentTimeMillis() - modelStart) + " ]\n" );

		// Texture Objects
		System.out.println( "----- Loading Textures -----\n" );
		long textureStart = System.currentTimeMillis();

		for ( File image : new File( "textures" ).listFiles() ) {
			System.out.println( image.getName() );
			try {
				Texture t = TextureIO.newTexture( image, true );
				t.setTexParameteri( gl, GL3.GL_TEXTURE_MIN_FILTER, GL3.GL_NEAREST );
				t.setTexParameteri( gl, GL3.GL_TEXTURE_MAG_FILTER, GL3.GL_NEAREST );
				Main._tl.add( t );
			} catch ( Exception e ) {
				System.out.println( e.getMessage() );
			}
		}
		System.out.println( "----- Textures Loaded! ----- [time : "
				+ (System.currentTimeMillis() - textureStart) + " ]\n" );

		// Font (Texture)

		System.out.println( "----- Loading Font -----" );
		long fontStart = System.currentTimeMillis();

		for ( File c : new File( "Font" ).listFiles() ){
			try {
				Texture t = TextureIO.newTexture( c, true );
				t.setTexParameteri( gl, GL3.GL_TEXTURE_MIN_FILTER, GL3.GL_NEAREST );
				t.setTexParameteri( gl, GL3.GL_TEXTURE_MAG_FILTER, GL3.GL_LINEAR );
				Main._fl.add( t );
			} catch ( Exception e ) {
				System.out.println( e.getMessage() );
			}
		}
		System.out.println( "----- Font Loaded! -----[time : "
				+ (System.currentTimeMillis() - fontStart) + " ]\n" );

		// Get Shader Ready!

		System.out.println( "----- Loading Shaders -----" );
		long shaderStart = System.currentTimeMillis();

		int vertShader = gl.glCreateShader( GL3.GL_VERTEX_SHADER   );
		int fragShader = gl.glCreateShader( GL3.GL_FRAGMENT_SHADER );

		String[] vertString = new String[] { vertexShader  };
		int[] vertLen = new int[] { vertString[0].length() };
		gl.glShaderSource( vertShader, 1, vertString, vertLen, 0 );

		String[] fragString = new String[] { fragmentShader };
		int[] fragLen = new int[] { fragString[0].length()  };
		gl.glShaderSource( fragShader, 1, fragString, fragLen, 0 );

		gl.glCompileShader( vertShader );
		gl.glCompileShader( fragShader );

		shaderProgram = gl.glCreateProgram();
		gl.glAttachShader( shaderProgram, vertShader );
		gl.glAttachShader( shaderProgram, fragShader );

		gl.glBindAttribLocation( shaderProgram, 0, "position" );
		gl.glBindAttribLocation( shaderProgram, 1, "color"    );

		gl.glLinkProgram( shaderProgram );

		textureUnLoc = gl.glGetUniformLocation( shaderProgram, "myTexture" );
		modelMatrixLoc = gl.glGetUniformLocation( shaderProgram, "modelMatrix" );
		worldMatrixLoc = gl.glGetUniformLocation( shaderProgram, "worldMatrix" );
		viewMatrixLoc  = gl.glGetUniformLocation( shaderProgram, "viewMatrix"  );
		
		System.out.println( "----- Shaders Loaded! ----- [time : "
				+ (System.currentTimeMillis() - shaderStart) + " ]\n" );

		// Enable Textures!

		gl.glEnable( GL3.GL_TEXTURE_2D );

		System.out.println( "_____OpenGL Loaded!_____ [time : "
				+ (System.currentTimeMillis() - openGLStart) + " ]\n" );


	}

	@Override
	public void reshape( GLAutoDrawable draw, int x, int y, int w, int h ) {
		this.fov = Math.atan( Math.tan( this.fov / 2.0d ) * h / this.height ) * 2.0d;
		this.width  = w;
		this.height = h;

		Main.width = w;
		Main.height = h;

		GL3 gl = draw.getGL().getGL3(); 
		gl.glViewport( x, y, w, h );

		for ( Resizable r : Main._rl ) {

			r.update( w, h );

		}
	}



}
