package main;

import java.util.LinkedList;
import java.util.Queue;

public class FrameRateCounter {
	
	private Queue<Long> _fs = new LinkedList<Long>();
	
	public FrameRateCounter() {
		
	}
	
	public void addFrame() {
		_fs.offer( System.currentTimeMillis() );
		if ( _fs.peek() + 1000l < System.currentTimeMillis() ) {
			_fs.remove();
		}
		
	}
	
	public int getFPS() {
		return this._fs.size();
		
	}
	
}
