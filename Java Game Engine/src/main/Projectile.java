package main;

import graphicObjects.Graphic;
import graphicObjects.shapes.Sphere;
import maths.Vec3;
import physicalObjects.SpatialObject;
import universe.space.SpatialLocation;

public class Projectile extends SpatialObject {

	private long created = System.currentTimeMillis();
	public static final long LifeSpan = 5000l;
	
	public Projectile( SpatialLocation sLoc, Vec3 vel ) {
		super( sLoc, 10.0f, vel );
		Graphic g = new Graphic( Main._gl.indexOfGraphic( Sphere.class ), Main._tl.get( "RedSample.png" ) , sLoc.get_rPos() );
		g.setScale( new Vec3( 0.05f ) );
		
		setGraphic( g );
		
	}

	@Override
	public void timeStep( float dT ) {
		super.timeStep(dT);
		if ( created + LifeSpan < System.currentTimeMillis() ) {
			// TODO needs to remove projectile from the space. Has thread issues.
			Main._scene.remove( getGraphic() );
			
		}
	}
	
	@Override
	public String getInfo() {
		return "I am shooty thing!";
		
	}
	
	
	
}
