package main;

import graphicObjects.Graphic;
import graphicObjects.GraphicList;
import graphicObjects.TextureList;
import graphicObjects.graphic2D.GraphicContainer;
import graphicObjects.graphic2D.text.FontList;
import graphicObjects.shapes.Sphere;
import gui.GUI;
import gui.guiObjects.GuiPicture;
import gui.guiObjects.bordered.GuiBorderedButton;
import hud.HUD;
import icon.Icon;

import java.util.ArrayList;

import physicalObjects.SpatialObject;
import maths.FloatMatrix;
import maths.Vec2;
import maths.Vec3;
import ship.Ship;
import universe.Universe;
import universe.UniversePopulator;
import universe.space.SpatialLocation;

public class Main {

	public static final long  TIME_STEP = 10l; // Milli Seconds
	public static final float TIME_PER_SECOND = 1.0f; // Seconds 

	public static VeryGui windowGui;

	public static Universe verse;

	public static GraphicList _gl = new GraphicList();
	public static TextureList _tl = new TextureList();
	public static FontList    _fl = new FontList();

	public static Camera camera = new Camera();
	public static ArrayList<Graphic> _scene = new ArrayList<Graphic>();

	public static GUI gui;
	public static ArrayList<Resizable> _rl = new ArrayList<Resizable>();

	public static HUD hud = new HUD();
	public static ArrayList<Icon> _icons = new ArrayList<Icon>();
	public static ArrayList<GraphicContainer> _windows = new ArrayList<GraphicContainer>();

	// TODO Should not keep these here, should be in GUI class somewhere.
	public static float width = 0.0f, height = 0.0f;
	public static FloatMatrix proj;

	// FPS
	public static FrameRateCounter frc = new FrameRateCounter();

	// Cursor
	public static GuiPicture cursor;
	public static GuiPicture cTrail0;
	public static GuiPicture cTrail1;
	public static GuiPicture cTrail2;

	// Ship

	public static Ship ship;
	public static Graphic space;
	public static Vec3 camRot = new Vec3();

	public static void main( String[] args ) throws InterruptedException {

		//System.exit( 0 );

		windowGui = new VeryGui();

		while ( !windowGui.glCanvas.getGLEventListenerInitState( windowGui.glCanvas.getGLEventListener( 0 ) ) );
		//windowGui.anim.start();

		gui = new GUI();
		_rl.add( gui );

		// GUI

		cursor = new GuiPicture( _tl.get( "CenterScreen.png" ), new Vec2(), new Vec2( 16, 16 ) );
		gui.addGuiObject( cursor );

		cTrail0 = new GuiPicture(_tl.get( "CenterScreen.png" ), new Vec2(), new Vec2( 28, 28 ) );
		cTrail1 = new GuiPicture(_tl.get( "CenterScreen.png" ), new Vec2(), new Vec2( 36, 36 ) );
		cTrail2 = new GuiPicture(_tl.get( "CenterScreen.png" ), new Vec2(), new Vec2( 42, 42 ) );

		gui.addGuiObject( cTrail0 );
		gui.addGuiObject( cTrail1 );
		gui.addGuiObject( cTrail2 );

		GuiPicture gp0 = new GuiPicture( _tl.get( "CenterScreen.png" ), new Vec2(), new Vec2( 64, 64 ) );
		gui.addGuiObject( gp0 );

		//GuiBorderedList gm0 =  new GuiBorderedList( new Vec2( 0, -300 ), new Vec2( 500, 100 ) );
		//gui.addGuiObject( gm0 );

		GuiBorderedButton gb0 =  new GuiBorderedButton( "Banana?", new Vec2( 512, -250 ), new Vec2( 512, 30 ) );
		GuiBorderedButton gb1 =  new GuiBorderedButton( "Banana?", new Vec2( 512, -280 ), new Vec2( 512, 30 ) );
		GuiBorderedButton gb2 =  new GuiBorderedButton( "Banana?", new Vec2( 512, -310 ), new Vec2( 512, 30 ) );
		GuiBorderedButton gb3 =  new GuiBorderedButton( "Banana?", new Vec2( 512, -340 ), new Vec2( 512, 30 ) );

		gui.addGuiObject( gb0 );
		gui.addGuiObject( gb1 );
		gui.addGuiObject( gb2 );
		gui.addGuiObject( gb3 );

		//gm0.addGuiObject( gb0 );
		//gb0.setCornerPosition( new Vec2( 100, 100 ) );

		// Universe

		UniversePopulator unG = new UniversePopulator();
		verse = unG.getVerse();

		ship = new Ship(
				new SpatialLocation(
					verse.getSpace().getCosmics().get( 0 ).get_rSpace(),
					Vec3.multiply(
							new Vec3( 0.0f, 0.0f, 100.0f ),
							verse.getSpace().getCosmics().get( 0 ).get_rSpace().getUnit().numMeters()
							)
					)
				);
		
		verse.getSpace().getCosmics().get( 0 ).get_rSpace().addShip( ship );

		camera.view( verse.getSpace().getCosmics().get( 0 ).get_rSpace().getAllGraphics() );
		hud.track( verse.getSpace().getCosmics().get( 0 ).get_rSpace().getAllSpatialObjects() );

		space = new Graphic(
				_gl.indexOfGraphic( Sphere.class ),
				_tl.get( "space2.jpg" ),
				new Vec3()
				);
		space.setScale( new Vec3( 200.0f ) );

		_scene.add( space );

		// Start Animator
		windowGui.anim.start();

		while ( true ) {

			ship.addRot( new Vec3( camRot.y * -0.00005f, camRot.x * 0.00005f, camRot.z ) );
			ship.timeStep( 0.01f );

			//gb0.setText( "Pos   : " + ship.get_sLoc().get_rPos() );
			gb0.setText( "FPS   : " + frc.getFPS()    );
			gb1.setText( "Vel   : " + ship.getVel()   );
			gb2.setText( "Acc   : " + ship.getAcc()   );
			gb3.setText( "Force : " + ship.getForce() );

			for ( SpatialObject so : verse.getSpace().getCosmics().get( 0 ).get_rSpace().getProj() ) {
				so.timeStep( 0.01f );

			}

			Thread.sleep( TIME_STEP );

		}

	}

}