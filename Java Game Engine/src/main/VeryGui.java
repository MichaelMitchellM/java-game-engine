package main;


import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import interaction.ClientKeyListener;
import interaction.ClientMouseListener;
import interaction.ClientMouseMotionListener;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;

import com.jogamp.opengl.util.FPSAnimator;

/**
 * 
 * @author Michael Mitchell
 * 
 * Makes a new JFrame with buttons and the OpenGL canvas.
 * Also has variables that are modified by the ActionListeners
 * these variables are used in the OpenGL render to translate and rotate.
 * 
 */

@SuppressWarnings("serial")
public class VeryGui extends JFrame {

	public GLCanvas glCanvas;

	public FPSAnimator anim;

	public VeryGui(){
		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		// setUndecorated( true );
		//setLayout( new GridBagLayout() );

		//Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		// setSize( screenSize.width, screenSize.height );

		// Black Cursor
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor( cursorImg, new Point(0, 0), "blank cursor" );
		getContentPane().setCursor(blankCursor);

		setSize( 512, 512 );

		GLProfile glProfile = GLProfile.getDefault();

		GLCapabilities glcapabilities = new GLCapabilities( glProfile );
		glCanvas = new GLCanvas( glcapabilities );

		glCanvas.addGLEventListener( new MyGLEventListener( this ) );
		
		glCanvas.addMouseListener( new ClientMouseListener() );
		glCanvas.addMouseMotionListener( new ClientMouseMotionListener() );
		glCanvas.addKeyListener( new ClientKeyListener() );

		add( glCanvas );

		anim = new FPSAnimator( glCanvas, 60 );

		setVisible( true );

	}
	
}
