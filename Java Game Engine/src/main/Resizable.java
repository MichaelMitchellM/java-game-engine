package main;

public interface Resizable {
	
	void update( int width, int height );
	
}
