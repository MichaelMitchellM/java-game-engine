package physics;

import physicalObjects.SpatialObject;
import universe.builder.SpatialObjectBuilder;
import maths.Vec3;

public class Oribt {

	
	private Vec3 periapsis;
	private Vec3 apoapsis;
	
	private SpatialObjectBuilder<SpatialObject> sob1;
	private SpatialObjectBuilder<SpatialObject> sob2;
	
	public Oribt( Vec3 peri, Vec3 apoa,
			SpatialObjectBuilder<SpatialObject> s1, SpatialObjectBuilder<SpatialObject> s2
			) {
		
		this.periapsis = peri;
		this.apoapsis = apoa;
		
		this.sob1 = s1;
		this.sob2 = s2;
		
		
		
	}
	
}
