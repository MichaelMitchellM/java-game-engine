package physics;

import maths.Vec3;

public class Gravity {
	
	public static final float G = 6.67384E-11f;
	
	public static float getFG( float mass1, float mass2, float distance ){
		// mass1 & mass2 in Kg, distance in meters.
		return ( G * (mass1 / distance) * (mass2 / distance) );
	}
	
	public static Vec3 getOrbitalVel( float mass, float distance ){
		float vel = (float) Math.sqrt( ( G * mass ) / distance );
		
		return new Vec3( 0.0f, vel, 0.0f ); // TODO this needs to be changed.
	}
	
}
