package physics;

import maths.Vec3;

public class Kinematics {

	// Uses Vec3 instead of doing all of the individual x, y, z.

	// Possibly allow instances to be made, for events and things.
	// For now, just a place for static methods to do the Kinematic Calculations.

	public static Vec3 get_dPos( Vec3 vel, Vec3 acc, float dT ){
		Vec3 dPos = new Vec3();
		dPos.x = (vel.x * dT) + (0.5f * acc.x * dT*dT);
		dPos.y = (vel.y * dT) + (0.5f * acc.y * dT*dT);
		dPos.z = (vel.z * dT) + (0.5f * acc.z * dT*dT);
		return dPos;
	}

	public static Vec3 get_fVel( Vec3 vel, Vec3 acc, float dT ){
		Vec3 fVel = new Vec3();
		fVel.x = vel.x + (acc.x * dT);
		fVel.y = vel.y + (acc.y * dT);
		fVel.z = vel.z + (acc.z * dT);
		return fVel;
	}

}