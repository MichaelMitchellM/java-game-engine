package units;

public enum Units {
	
	METER( 1.0f ),
	KILOMETER( 1000.0f ),
	AU( 149597870700.0f ),
	LIGHTYEAR( 9460730472580800.0f );
	
	private float numMeters;
	
	private Units( float numMeters ){
		this.numMeters = numMeters;
		
	}
	
	public float numMeters(){
		return this.numMeters;
		
	}
	
}
