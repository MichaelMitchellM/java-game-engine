package interaction;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import main.Main;
import maths.Vec2;

public class ClientMouseMotionListener implements MouseMotionListener {

	@Override
	public void mouseDragged(MouseEvent e) {
		mouseMoved( e ); // TODO not too sure about this...
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		float gX = (float) (e.getPoint().getX() - Main.windowGui.glCanvas.getWidth()/2.0f);
		float gY = (float) (-1 * e.getPoint().getY() + Main.windowGui.glCanvas.getHeight()/2.0f);
		Main.camRot.x = gX;
		Main.camRot.y = gY;
		
		Main.cursor.setPosition( new Vec2( gX, gY ) );
		
		Main.cTrail0.setPosition( new Vec2( gX/1.5f, gY/1.5f ) );
		Main.cTrail1.setPosition( new Vec2( gX/2, gY/2 ) );
		Main.cTrail2.setPosition( new Vec2( gX/3, gY/3 ) );
		
	}

}
