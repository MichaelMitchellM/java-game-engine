package interaction;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import main.Main;
import maths.Vec3;

public class ClientKeyListener implements KeyListener {

	@Override
	public void keyPressed(KeyEvent e) {

		if ( e.getKeyChar() == 'w' ) {
			Main.ship.setForward( 1E20f );

		} else if ( e.getKeyChar() == 's' ) {
			Main.ship.setForward( -1E20f );

		} else if ( e.getKeyChar() == 'a' ) {
			Main.camRot.z = -0.05f;

		} else if ( e.getKeyChar() == 'd' ) {
			Main.camRot.z = 0.05f;

		} else if ( e.getKeyChar() == 'q' ) {

		} else if ( e.getKeyChar() == 'e' ) {

		} else if ( e.getKeyChar() == ' ' ) {
			Main.ship.shoot();

		} else if ( e.getKeyCode() == 17 ) {
			Main.ship.applyJello();

		}

	}

	@Override
	public void keyReleased(KeyEvent e) {

		if ( e.getKeyChar() == 'w' ) {
			Main.ship.setForce( new Vec3() );

		} else if ( e.getKeyChar() == 's' ) {
			Main.ship.setForce( new Vec3() );

		} else if ( e.getKeyChar() == 'a' ) {
			Main.camRot.z = 0.0f;

		} else if ( e.getKeyChar() == 'd' ) {
			Main.camRot.z = 0.0f;

		} else if ( e.getKeyChar() == 'q' ) {

		} else if ( e.getKeyChar() == 'e' ) {

		} else if ( e.getKeyChar() == ' ' ) {
			
		} else if ( e.getKeyCode() == 17 ) {
			Main.ship.setForce( new Vec3() );

		}

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}



}
