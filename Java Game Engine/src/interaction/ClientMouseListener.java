package interaction;

import graphicObjects.graphic2D.text.TextArea;
import gui.GuiObject;
import gui.guiObjects.bordered.GuiBorderedList;
import icon.Icon;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import main.Main;
import maths.Vec2;
import maths.Vec3;

public class ClientMouseListener implements MouseListener {

	@Override
	public void mouseClicked( MouseEvent e ) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {
		Main.camRot = new Vec3( 0.0f, 0.0f, 0.0f );
		
		Main.cursor.setPosition( new Vec2() );
		Main.cTrail0.setPosition( new Vec2() );
		Main.cTrail1.setPosition( new Vec2() );
		Main.cTrail2.setPosition( new Vec2() );
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		float gX = e.getX() - Main.windowGui.glCanvas.getWidth()/2.0f;
		float gY = -1 * e.getY() + Main.windowGui.glCanvas.getHeight()/2.0f;
		for ( GuiObject go : Main.gui.getGuiObjects() ) {
			Vec2 p = go.getPosition();
			Vec2 d = go.getDimensions();
			if ( gX > p.x - d.x/2.0f && gX < p.x + d.x/2.0f ) {
				if ( gY > p.y - d.y/2.0f && gY < p.y + d.y/2.0f ) {
					
					Main.gui.getGuiObjects().remove( go );
					Main.gui.getGuiObjects().add( 0, go );
					
					if ( go instanceof GuiBorderedList ) { // TODO this should be part of GuiBorderedMenu or something?
						for ( GuiObject sgo : ((GuiBorderedList) go).getGuiObjects() ) {
							Main.gui.getGuiObjects().remove( sgo );
							Main.gui.getGuiObjects().add( 0, sgo );
							
						}
					}
					
					go.interactAction();
					return;
					
				}
			}
		}
		
		for ( Icon i : Main._icons ) {
			if ( gX > i.getIconImage().getPosition().x - i.getIconImage().getScale().x && gX < i.getIconImage().getPosition().x + i.getIconImage().getScale().x ) {
				if ( gY > i.getIconImage().getPosition().y - i.getIconImage().getScale().y && gY < i.getIconImage().getPosition().y + i.getIconImage().getScale().y ) {
					Main._windows.add(
							new TextArea(
									(int) i.getIconImage().getPosition().x,
									(int) i.getIconImage().getPosition().y,
									i.getRef().getInfo()
									)
							);
					break;
					
				}
			}
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}


}
