package interaction;

import maths.Vec2;

public interface Interactable {

	void interactAction();
	
	void setCornerPosition( Vec2 p );
	void setInnerCornerPosition( Vec2 p );
	void setPosition( Vec2 p );
	void setDimensions( Vec2 d );
	void setInnerDimensions( Vec2 d );
	
	Vec2 getCornerPosition();
	Vec2 getInnerCornerPosition();
	Vec2 getPosition();
	Vec2 getDimensions();
	Vec2 getInnerDimensions();
	
}
